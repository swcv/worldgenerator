#!/usr/bin/env python3
#
# Author: Jarkko Vatjus-Anttila <jvatjusanttila@gmail.com>
#
# For conditions of distribution and use, see copyright notice in license.txt
#

import sys, os, io
import random
import numpy
import math

from PIL import Image

class TextureGenerator():
    """ class TextureGenerator(): A collection of procedural methods for
        creating texture files with various content.
        Uses Python Imaging Library for image I/O
    """

    def __init__(self, width=256, height=256):
        """ TextureGenerator.__init__(width, height):
        """
        self.initialize(width, height)
        self.verbose = False

    def initialize(self, width=256, height=256):
        """ TextureGenerator.initialize(width, height):
        """
        self.width = width
        self.height = height
        self.r_array = numpy.zeros([width,height], dtype=float)
        self.g_array = numpy.zeros([width,height], dtype=float)
        self.b_array = numpy.zeros([width,height], dtype=float)
        self.a_array = numpy.zeros([width,height], dtype=float)

    def enableVerbose(self):
        self.verbose = True

#############################################################################
# Texture textual output methods
#

    def printMessage(self, message):
        if self.verbose == False: return
        sys.stdout.write("TextureGenerator: " + message + "\n")

    def printError(self, message):
        #if self.verbose == False: return
        sys.stderr.write("TextureGenerator ERROR: " + str(message) + "\n")

#############################################################################
# Image I/O with PIL
#

    def fromImage(self, imagefile):
        """ TextureGenerator.fromImage(self, imagefile):
        """
        try: image = Image.open(imagefile)
        except IOError:
            self.printError("File "+str(imagefile)+" open failed. Aborting");
            return False

        imagesize = image.size
        self.printMessage("image x,y %d,%d" % (imagesize[0], imagesize[1]))

        pixels = image.load()
        self.initialize(imagesize[0], imagesize[1])
        for i in range(imagesize[0]):
            for j in range(imagesize[1]):
                self.r_array[i][j] = float(pixels[i,j][0])/255.0
                self.g_array[i][j] = float(pixels[i,j][1])/255.0
                self.b_array[i][j] = float(pixels[i,j][2])/255.0
                try:
                    self.a_array[i][j] = float(pixels[i,j][3])/255.0
                except IndexError:
                    self.a_array[i][j] = 1.0
                #print "reading ", self.r_array[i][j], self.g_array[i][j], self.b_array[i][j], self.a_array[i][j]
        return True

    def toImage(self, filename, fileformat="TGA", overwrite=False, alpha=False):
        """ TextureGenerator.toImage(self, filename, fileformat="TGA", overwrite=False):
        """
        if os.path.exists(filename):
            if overwrite == False:
                self.printError("Requested output file " + str(filename) + " already exists. Aborting.")
                return False
            os.remove(filename)
        try: f = open(filename, "wb")
        except IOError:
            self.printError("Failed to open file " + str(filename) + ". Aborting!")
            return False

        if alpha == True: datatype="RGBA"
        else: datatype="RGB"

        image = Image.new(datatype, (self.width, self.height))
        data = []
        for i in range(self.width):
            for j in range(self.height):
                r = max(0,min(255, int(255.0*self.r_array[j][i]))) # this mirrored?
                g = max(0,min(255, int(255.0*self.g_array[j][i])))
                b = max(0,min(255, int(255.0*self.b_array[j][i])))
                a = max(0,min(255, int(255.0*self.a_array[j][i])))
                #print "writing", r, g, b, a
                data.append(a*256*256*256 + b*256*256 + g*256 + r)

        image.putdata(data)
        image.save(filename, fileformat)
        return True

#############################################################################
# TextureGenerator get/putPixel:
#

    def getPixel(self, x, y):
        r = self.r_array[x*self.width][y*self.height]
        g = self.g_array[x*self.width][y*self.height]
        b = self.b_array[x*self.width][y*self.height]
        a = self.a_array[x*self.width][y*self.height]
        return r, g, b, a

    def putPixel(self, x, y, r, g, b, a):
        self.r_array[x*self.width][y*self.height] = r
        self.g_array[x*self.width][y*self.height] = g
        self.b_array[x*self.width][y*self.height] = b
        self.a_array[x*self.width][y*self.height] = a

#############################################################################
# TextureGenerator macros:
#

    def createSingleColorTexture(self, r=1.0, g=1.0, b=1.0, a=1.0, variance=0.0):
        """ TextureGenerator.createSingleColorTexture(self, r, g, b, variance):
        """
        for i in range(self.width):
            for j in range(self.height):
                var = variance*(random.random()-0.5)
                self.r_array[i][j] = r + var
                self.g_array[i][j] = g + var
                self.b_array[i][j] = b + var
                self.a_array[i][j] = a + var

    #######################################################################
    # fresnelCoefficients():
    #
    # In Cook-Torrance illumation model, the surface specular reflectivity
    # can be derived to depend only on surface normal (N) and camera halfway
    # vector (H) angle as well as light source direction (L) and halfway vector
    # (H) angle. The complete illumination model has several contributors, but
    # among those the Fresnel factor and Beckmann microfacet distribution
    # funcion can be stored in a two dimensional lookup texture, which is
    # essentially calculated here.
    #
    # Input parameters are the color of wanted specular reflectivity and
    # surface roughness in range [0.0 .. 1.0]
    #
    # Special note:
    #  1) This texture can be fed directly into Neocortex Cook-Torrance surface
    #     shader. It may or may not be supported by any other rendering engine
    #     without modifications.
    #  2) The calculation below supports construction of HDR illumination effect,
    #     but it is clamped to regular RGB range when the data is written to a file.
    #     If HDR texture is required, an alternative data storage is neede.
    #
    def fresnelCoefficients(self, specR, specG, specB, R):
        #
        print ("Calculating Fresnel co-efficients for specular color <%f,%f,%f>, surface Roughness=%f" % (specR, specG, specB, R))
        #
        # local macro for fresnel coef calculation:
        #
        def __fresnelCOEF(LH, g):
            try:
                f  = (LH*(g+LH)-1)*(LH*(g+LH)-1) / ((LH*(g-LH)+1)*(LH*(g-LH)+1)) + 1
                f *= 0.5*(g-LH)*(g-LH)/((g+LH)*(g+LH))
                return f
            except ZeroDivisionError:
                return 0.0
        #
        # Because, for low values of surface roughness, the most relevant information
        # starts to appear only with values of NdotH which are close to 1.0. Hence,
        # We iterate a spot between 0.0 .. 1.0, where the change in illumation is
        # roughly the value of "epsilon", and scale the texture according to that.
        # Is is estimated that all illumination values for this threshold are
        # zero.
        #
        epsilon = 0.005
        NH = 1.0
        iteration = 0.25
        while 1:
            value = 1.0/(4*R*R*NH*NH*NH*NH)*math.exp((NH*NH-1)/(R*R*NH*NH))
            #print NH, iteration, value, abs(value-epsilon), epsilon
            NH += abs(value-epsilon)/(epsilon-value)*iteration
            iteration /= 2.0
            if abs(value-epsilon) < 0.00000001: break
            if iteration < 0.0000000001: break
        #
        print ("Calculated NdotH scaling value: %f" % NH)
        startNH = NH
        startLH = 0.0
        stepNH  = (1.0-startNH)/self.width
        stepLH  = (1.0-startLH)/self.height
        self.printMessage("Fresnel steps: %f %f" % (stepNH, stepLH))
        #
        try: Nr = (1+math.sqrt(specR))/(1-math.sqrt(specR))
        except ZeroDivisionError: Nr = -1.0
        try: Ng = (1+math.sqrt(specG))/(1-math.sqrt(specG))
        except ZeroDivisionError: Ng = -1.0
        try: Nb = (1+math.sqrt(specB))/(1-math.sqrt(specB))
        except ZeroDivisionError: Nb = -1.0
        self.printMessage("Fresnel factors: %f %f %f" % (Nr, Ng, Nb))
        #
        NH = startNH
        LH = startLH
        for y in range(self.height):
            for x in range(self.width):
                #
                # 1) First, Beckmann microfacet estimation, based on surface roughness:
                try:
                    microfacetDF = 1.0/(4*R*R*NH*NH*NH*NH)*math.exp((NH*NH-1)/(R*R*NH*NH))
                except ZeroDivisionError: microfacetDF = 0.0
                #
                # 2) Second, the fresnel co-efficients for each major visible light wavelengths
                fresnelR = __fresnelCOEF(LH, math.sqrt(Nr*Nr-1+LH*LH))
                fresnelG = __fresnelCOEF(LH, math.sqrt(Ng*Ng-1+LH*LH))
                fresnelB = __fresnelCOEF(LH, math.sqrt(Nb*Nb-1+LH*LH))
                #print "<%d,%d>: %f - %f %f %f" % (x, y, microfacetDF, fresnelR, fresnelG, fresnelB)
                #
                # 3) Third, the lookup table will hold a product of the above for
                #    each wavelength
                self.r_array[self.height-y-1][x] = fresnelR*microfacetDF/math.pi
                self.g_array[self.height-y-1][x] = fresnelG*microfacetDF/math.pi
                self.b_array[self.height-y-1][x] = fresnelB*microfacetDF/math.pi
                #
                NH += stepNH
            LH += stepLH
            NH = startNH

    #######################################################################
    # normalMap():
    #

    def normalMap(self, basefile, coefR=1.0, coefG=1.0, coefB=1.0):
        basemap = TextureGenerator(8,8)
        self.fromImage(basefile)
        self.toGrayscale(coefR, coefG, coefB)
        self.toImage("resources/grayscale.png", "PNG", overwrite=True)
        #basemap.fromImage("resources/grayscale.png")
        #basemap.toImage("resources/grayscale2.png", "PNG", overwrite=True)
        gridSize = 1.0
        heightMap = list(self.r_array) # Make a temp copy
        for x in range(self.width):
            for y in range(self.height):
                #print "---", heightMap[x][y]
                N = [0, 0, 0]
                #
                deltaX = gridSize
                deltaY = 0.0
                if x > 0:
                    deltaZ = heightMap[x][y] - heightMap[x-1][y]
                    N[0] += -deltaZ*deltaX
                    N[1] += 0.0
                    N[2] += 1.0-deltaZ*deltaZ
                    #if N[2] < 0: print "ERROR at ", x, y
                    #print "1:", deltaX, deltaY, deltaZ, N
                #
                if x < self.width-1:
                    deltaZ = heightMap[x+1][y] - heightMap[x][y]
                    #print heightMap[x+1][y],heightMap[x][y]
                    N[0] += -deltaZ*deltaX
                    N[1] += 0.0
                    N[2] += 1.0-deltaZ*deltaZ
                    #if N[2] < 0: print "ERROR at ", x, y
                    #print "2:", deltaX, deltaY, deltaZ, N
                #
                deltaX = 0.0
                deltaY = gridSize
                if y > 0:
                    deltaZ = heightMap[x][y] - heightMap[x][y-1]
                    N[0] += 0.0
                    N[1] += -deltaZ*deltaY
                    N[2] += 1.0-deltaZ*deltaZ
                    #if N[2] < 0: print "ERROR at ", x, y
                    #print "3:", deltaX, deltaY, deltaZ, N
                #
                if y < self.height-1:
                    deltaZ = heightMap[x][y+1] - heightMap[x][y]
                    #print deltaZ
                    N[0] += 0.0
                    N[1] += -deltaZ*deltaY
                    N[2] += 1.0-deltaZ*deltaZ
                    #if N[2] < 0: print "ERROR at ", x, y
                    #print "4:", deltaX, deltaY, deltaZ, N
                #
                l = math.sqrt(N[0]*N[0] + N[1]*N[1] + N[2]*N[2])
                N[0] /= l
                N[1] /= l
                N[2] /= l
                #print "%d,%d - %f - %f %f %f" % (x, y, heightMap[x][y], N[0], N[1], N[2])
                self.r_array[x][y] = (N[0]+1.0)/2.0
                self.g_array[x][y] = (N[1]+1.0)/2.0
                self.b_array[x][y] = (N[2]+1.0)/2.0
        self.toImage("resources/normalmap.png", "PNG", overwrite=True)

    def toGrayscale(self, coefR=1.0, coefG=1.0, coefB=1.0):
        for x in range(self.width):
            for y in range(self.height):
                average = (coefR*self.r_array[x][y] +
                           coefG*self.g_array[x][y] +
                           coefB*self.b_array[x][y]) / (coefR + coefG + coefB)
                self.r_array[x][y] = average
                self.g_array[x][y] = average
                self.b_array[x][y] = average

#############################################################################
# Standalone test case
#

if __name__ == "__main__": # if run standalone
    texture = TextureGenerator(128,128)

    if 0:
        print ("Running unit test, generating grass texture...")
        texture.createSingleColorTexture(0.1,0.5,0.25,0.45)
        texture.fromImage("./resources/bricks.tga")

        print ("Writing, without alpha")
        texture.toImage("./resources/generated1.png", "PNG", overwrite=True)
        print ("Reading back...")
        texture.fromImage("./resources/generated1.png")
        print ("Writing, with alpha")
        texture.toImage("./resources/generated2.png", "PNG", overwrite=True, alpha=True)
        print ("Reading back...")
        texture.fromImage("./resources/generated2.png")
        print ("Writing, without alpha")
        texture.toImage("./resources/generated3.png", "PNG", overwrite=True, alpha=False)

    #texture.fresnelCoefficients(0.8, 0.6, 0.1, 0.2)
    #texture.toImage("./resources/fresnel.png", "PNG", overwrite=True, alpha=False)

    texture.normalMap("./resources/heightmap.png")


