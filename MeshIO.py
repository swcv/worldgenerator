#!/usr/bin/env python3 
#
# Author: Jarkko Vatjus-Anttila <jvatjusanttila@gmail.com>
#
# For conditions of distribution and use, see copyright notice in license.txt
#

import sys, os
import xml.sax
import MeshContainer

import struct
import numpy

#############################################################################
# Mesh IO classes for manipulation for inport, export of the mesh data
# All classes defined in this file work through the MeshContainer class.
#
# Implemented classes:
#  - MeshIO                 - Helper wrapper for basic IO functionality
#  - OgreXMLImport          - Private class for Ogre XML importing
#  - OgreXMLExport          - Private class for Ogre XML exporting
#  - OgreMeshImport         - Private class for Ogre binary mesh import
#  - VTKImport              - Private class for VTK ASCII mesh import
#  - NMESHImport            - Native neocortex mesh de-serializer
#  - NMESHExport            - Native neocortex mesh serializer
#

class MeshIO():
    def __init__(self, meshcontainer):
        self.meshcontainer = meshcontainer

    def fromFile(self, localfile, mimetype="model/x-ogremesh"):
        if mimetype == "model/mesh" or localfile.lower().endswith(".mesh"):
            ml = OgreMeshImport(self.meshcontainer)
        elif mimetype == "model/x-ogremesh" or localfile.lower().endswith(".xml"):
            ml = OgreXMLImport(self.meshcontainer)
        elif mimetype == "model/vtk" or localfile.lower().endswith(".vtk"):
            ml = VTKMeshImport(self.meshcontainer)
        elif mimetype == "model/obj" or localfile.lower().endswith(".obj"):
            ml = OBJMeshImport(self.meshcontainer)
        elif mimetype == "model/nmesh" or localfile.lower().endswith(".nmesh"):
            ml = NMESHImport(self.meshcontainer)
        else:
            print ("Unknown mimetype %s. Import Aborted!" % item.mimetype)
            return
        # load data:
        ml.fromFile(localfile)

    def toFile(self, localfile, overwrite=False, positions=True, normals=True, texcoords0=True, texcoords1=True, diffusecolors=True, tangents=True):
        if os.path.exists(localfile):
            if overwrite == False:
                print ("Output file %s already exists, abort" % localfile)
                return
            try: os.unlink(localfile)
            except IOError:
                print ("Cannot overwrite file %s. Abort!" % localfile)
                return
        if localfile.endswith(".xml"):
            ml = OgreXMLExport(self.meshcontainer, localfile, overwrite)
            ml.toFile(localfile, positions=positions, normals=normals, texcoords0=texcoords0, texcoords1=texcoords1, diffusecolors=diffusecolors, tangents=tangents)
        elif localfile.endswith(".json"):
            ml = JSONExport(self.meshcontainer, localfile, overwrite)
            ml.toFile(localfile, positions=positions, normals=normals, texcoords0=texcoords0, texcoords1=texcoords1, diffusecolors=diffusecolors, tangents=tangents)
        elif localfile.endswith(".nmesh"):
            ml = NMESHExport(self.meshcontainer, localfile, overwrite)
            ml.toFile(localfile, positions=positions, normals=normals, texcoords0=texcoords0, texcoords1=texcoords1, diffusecolors=diffusecolors, tangents=tangents)
        elif localfile.endswith(".obj"):
            ml = OBJExport(self.meshcontainer, localfile, overwrite)
            ml.toFile(localfile, positions=positions, normals=normals, texcoords0=texcoords0, texcoords1=texcoords1, diffusecolors=diffusecolors, tangents=tangents)
        else:
            print ("Unknown file ending '%s'. Abort!" % localfile[-4:])

    def build3DTextures(self, prefix="3dtex%d.bin", overwrite=False):
        count = 0
        for m in self.meshcontainer.submeshes:
            filename = prefix % count
            count += 1
            m.vertexBuffer.build3DTexture(size=32, filename=filename)

#############################################################################
# OgreXMLImport class
#
class OgreXMLImport():
    """ OgreXMLImport() is able to eat Ogre XML input and fill in the provided
        MeshContainer class for further use.
    """
    def __init__(self, meshcontainer):
        self.meshcontainer = meshcontainer

    ###
    # OgreXMLImport: I/O
    #
    def fromFile(self, localfile):
        p = xml.sax.make_parser()
        ogreparser = OgreXMLImport.XMLParser()
        ogreparser.setMeshContainer(self.meshcontainer)
        p.setContentHandler(ogreparser)
        p.parse(localfile)

    ###
    # OgreXMLImport: XMLParser: XML parser class for OgreXML fileformat:
    #
    class XMLParser(xml.sax.ContentHandler):
        def setMeshContainer(self, container):
            self.mc = container
            self.texcoordBank = 0

        ###
        # Start element methods:
        #
        def __start_mesh(self, attributes):
            pass
        def __start_submeshes(self, attributes):
            pass
        def __start_submesh(self, attributes):
            materialref = ""
            operationtype = "triangle_list"
            try: materialref = attributes.getValueByQName("material")
            except KeyError: materialref = ""
            try: operationtype = attributes.getValueByQName("operationtype")
            except KeyError: operationtype = "triangle_list"
            self.mc.newSubmesh(materialref, operationtype)
        def __start_sharedgeometry(self, attributes):
            self.mc.newSharedGeometry()
        def __start_faces(self, attributes):
            pass
        def __start_face(self, attributes):
            self.mc.addFace([int(attributes.getValueByQName("v1")),
                             int(attributes.getValueByQName("v2")),
                             int(attributes.getValueByQName("v3"))])
        def __start_geometry(self, attributes):
            pass
        def __start_vertex(self, attributes):
            self.texcoordBank = 0
        def __start_vertexbuffer(self, attributes):
            for i in range(8):
                try:
                    value = attributes.getValueByQName("texture_coord_dimensions_%d" % i)
                except KeyError:
                    break
                if value == "float2":
                    self.mc.setTexcoordDimensions(i, 2)
                if value == "float3":
                    self.mc.setTexcoordDimensions(i, 3)
                if value == "float4":
                    self.mc.setTexcoordDimensions(i, 4)
        def __start_position(self, attributes):
            self.mc.addVertex([float(attributes.getValueByQName("x")),
                               float(attributes.getValueByQName("y")),
                               float(attributes.getValueByQName("z"))])
        def __start_normal(self, attributes):
            self.mc.addNormal([float(attributes.getValueByQName("x")),
                               float(attributes.getValueByQName("y")),
                               float(attributes.getValueByQName("z"))])
        def __start_texcoord(self, attributes):
            names = ["u", "v", "w", "x"]
            coords = []
            for a in names:
                try:
                    coords.append(float(attributes.getValueByQName(a)))
                except KeyError:
                    break
            self.mc.addTexcoord(coords, self.texcoordBank)
            self.texcoordBank += 1
        def __start_diffusecolor(self, attributes):
            self.mc.addDiffuseColor(attributes.getValueByQName("value"))
        def __start_boneassignments(self, attributes):
            self.mc.newBoneAssignments()
        def __start_vertexboneassignment(self, attributes):
            self.mc.addVertexBoneAssignment([])
        def __start_skeletonlink(self, attributes):
            self.mc.addSkeletonLink()
        def __start_submeshnames(self, attributes):
            pass
        def __start_submeshname(self, attributes):
            self.mc.addSubmeshName(attributes.getValueByQName("name"),
                                   attributes.getValueByQName("index"))

        ###
        # End element methods:
        #
        def __end_mesh(self):
            pass
        def __end_submeshes(self):
            pass
        def __end_submesh(self):
            pass
        def __end_sharedgeometry(self):
            pass
        def __end_faces(self):
            pass
        def __end_face(self):
            pass
        def __end_geometry(self):
            pass
        def __end_vertexbuffer(self):
            pass
        def __end_position(self):
            pass
        def __end_normal(self):
            pass
        def __end_texcoord(self):
            pass
        def __end_diffusecolor(self):
            pass
        def __end_boneassignments(self):
            pass
        def __end_vertexboneassignment(self):
            pass
        def __end_skeletonlink(self):
            pass
        def __end_submeshnames(self):
            pass
        def __end_submeshname(self):
            pass

        def startElement(self, tag, attributes):
            if tag == "mesh":                 return self.__start_mesh(attributes)
            if tag == "submeshes":            return self.__start_submeshes(attributes)
            if tag == "submesh":              return self.__start_submesh(attributes)
            if tag == "sharedgeometry":       return self.__start_sharedgeometry(attributes)
            if tag == "faces":                return self.__start_faces(attributes)
            if tag == "face":                 return self.__start_face(attributes)
            if tag == "geometry":             return self.__start_geometry(attributes)
            if tag == "vertex":               return self.__start_vertex(attributes)
            if tag == "vertexbuffer":         return self.__start_vertexbuffer(attributes)
            if tag == "position":             return self.__start_position(attributes)
            if tag == "normal":               return self.__start_normal(attributes)
            if tag == "texcoord":             return self.__start_texcoord(attributes)
            if tag == "colour_diffuse":       return self.__start_diffusecolor(attributes)
            if tag == "boneassignmenst":      return self.__start_boneassignments(attributes)
            if tag == "vertexboneassignment": return self.__start_vertexboneassignment(attributes)
            if tag == "skeletonlink":         return self.__start_skeletonlink(attributes)
            if tag == "submeshnames":         return self.__start_submeshnames(attributes)
            if tag == "submeshname":          return self.__start_submeshname(attributes)

        def endElement(self, tag):
            if tag == "mesh":                 return self.__end_mesh()
            if tag == "submeshes":            return self.__end_submeshes()
            if tag == "submesh":              return self.__end_submesh()
            if tag == "sharedgeometry":       return self.__end_sharedgeometry()
            if tag == "faces":                return self.__end_faces()
            if tag == "face":                 return self.__end_face()
            if tag == "geometry":             return self.__end_geometry()
            if tag == "vertexbuffer":         return self.__end_vertexbuffer()
            if tag == "position":             return self.__end_position()
            if tag == "normal":               return self.__end_normal()
            if tag == "texcoord":             return self.__end_texcoord()
            if tag == "colour_diffuse":       return self.__end_diffusecolor()
            if tag == "boneassignmenst":      return self.__end_boneassignments()
            if tag == "vertexboneassignment": return self.__end_vertexboneassignment()
            if tag == "skeletonlink":         return self.__end_skeletonlink()
            if tag == "submeshnames":         return self.__end_submeshnames()
            if tag == "submeshname":          return self.__end_submeshname()

    ###
    # OgreXMLImport: debug
    #
    def printStatistics(self):
        self.meshcontainer.printStatistics()

#############################################################################
# JSONExport: XML3D JSON export output class
#
class JSONExport():
    """ class JSONExport(): I/O component for writing formatted XML3D JSON notation
    """
    def __init__(self, meshcontainer, localfile, overwrite=False):
        self.indent = 0
        self.outputJSONFile = None
        self.meshcontainer = meshcontainer
        self.__openOutputJSON(localfile, overwrite)

    def __openOutputJSON(self, localfile, overwrite):
        if os.path.exists(localfile):
            if overwrite == False:
                print ("JSONExport: ERROR: output file '%s' already exists!\n" % filename)
                return
            else:
                os.unlink(localfile)
        try: self.outputJSONFile = open(localfile, "w")
        except IOError:
            print ("JSONExport: ERROR: Unable to open file '%s' for writing!" % filename)
            self.outputJSONFile = None

    def __closeOutputJSON(self):
        self.outputJSONFile.close()
        self.outputJSONFile = None

    def __outputJSON(self, msg):
        if self.outputJSONFile == None: return
        indent_str = ""
        for i in range(self.indent):
            indent_str = indent_str + " "
        self.outputJSONFile.write((indent_str + str(msg) + "\n"))

    def toFile(self, localfile, positions=True, normals=True, texcoords0=True, texcoords1=True, diffusecolors=True):
        self.__outputJSON('{ "format" : "xml3d-json", "version" : "0.4.0", ')
        for m in self.meshcontainer.submeshes:
            self.__outputJSON('"data": {')
            self.__outputJSON('"index":{"type":"int","seq":[{"value":%s}]},' % m.faces)
            self.__outputJSON('"position":{"type":"float3","seq":[{"value":%s}]},' % m.vertexBuffer.vertices)
            self.__outputJSON('"normal":{"type":"float3","seq":[{"value":%s}]},' % m.vertexBuffer.normals)
            self.__outputJSON('"texcoord":{"type":"float2","seq":[{"value":%s}]}' % m.vertexBuffer.texcoords)
            self.__outputJSON('} }')
        self.__closeOutputJSON()
        return

        self.startMesh()
        if self.meshcontainer.sharedgeometry != None:
            self.startSharedgeometry(len(self.meshcontainer.sharedgeometry.vertices))
            self.__outputVertexbuffer(self.meshcontainer.sharedgeometry, positions, normals, texcoords0, texcoords1, diffusecolors)
            self.endSharedgeometry()

        if len(self.meshcontainer.submeshes) > 0:
            self.startSubmeshes()
            for m in self.meshcontainer.submeshes:
                longIndices = False
                if self.meshcontainer.sharedgeometry != None:
                    longIndices = len(self.meshcontainer.sharedgeometry.vertices) > 32768
                else:
                    longIndices = len(m.vertexBuffer.vertices) > 32768
                self.startSubmesh(m.materialref, (self.meshcontainer.sharedgeometry!=None), longIndices, m.operationtype)

                self.startFaces(len(m.faces))
                for f in m.faces: #[m.faces[x:x+3] for x in range(0, len(m.faces), 3)]:
                    self.outputFace(f)
                self.endFaces()

                if (len(m.vertexBuffer.vertices) > 0):
                    self.startGeometry(len(m.vertexBuffer.vertices))
                    self.__outputVertexbuffer(m.vertexBuffer, positions, normals, texcoords0, texcoords1, diffusecolors)
                    self.endGeometry()

                self.endSubmesh()
            self.endSubmeshes()
        self.endMesh()
        self.closeOutputXML()

#############################################################################
# OgreXMLExport: Ogre XML output class
#
class OgreXMLExport():
    """ class OgreXMLExport(): I/O component for writing formatted OgreXML notation
        - Primarily used by MeshGenerator for producing Ogre compliant XML mesh
          notation.
        - All XML requests are pushed into buffer, and once toFile method is called
          the contents are flushes to the desired output file.
    """
    def __init__(self, meshcontainer, localfile, overwrite=False):
        self.indent = 0
        self.outputXMLFile = None
        self.meshcontainer = meshcontainer
        self.openOutputXML(localfile, overwrite)

    def toFile(self, localfile, positions=True, normals=True, texcoords0=True, texcoords1=True, diffusecolors=True, tangents=True):
        self.startMesh()
        if self.meshcontainer.sharedgeometry != None:
            self.startSharedgeometry(len(self.meshcontainer.sharedgeometry.vertices))
            self.__outputVertexbuffer(self.meshcontainer.sharedgeometry, positions, normals, texcoords0, texcoords1, diffusecolors, tangents)
            self.endSharedgeometry()

        if len(self.meshcontainer.submeshes) > 0:
            self.startSubmeshes()
            for m in self.meshcontainer.submeshes:
                if self.meshcontainer.sharedgeometry != None:
                    longIndices = len(self.meshcontainer.sharedgeometry.vertices) > 32768
                else:
                    longIndices = len(m.vertexBuffer.vertices) > 32768
                self.startSubmesh(m.materialref, (self.meshcontainer.sharedgeometry!=None), longIndices, m.operationtype)

                self.startFaces(len(m.faces))
                for f in m.faces: #[m.faces[x:x+3] for x in range(0, len(m.faces), 3)]:
                    self.outputFace(f)
                self.endFaces()

                if (len(m.vertexBuffer.vertices) > 0):
                    self.startGeometry(len(m.vertexBuffer.vertices))
                    self.__outputVertexbuffer(m.vertexBuffer, positions, normals, texcoords0, texcoords1, diffusecolors, tangents)
                    self.endGeometry()

                self.endSubmesh()
            self.endSubmeshes()
        self.endMesh()
        self.closeOutputXML()

    def __outputVertexbuffer(self, vb, positions=True, normals=True, texcoords0=True, texcoords1=False, diffusecolors=True, tangents=True):
        vb.debugMinMax()
        #tdim0 = vb.texcoordDimensions[0]
        #tdim1 = vb.texcoordDimensions[1]
        #cDim  = vb.diffusecolorDimensions;
        #vVector  = [vb.vertices[x:x+3]           for x in range(0, len(vb.vertices), 3)]
        #nVector  = [vb.normals[x:x+3]            for x in range(0, len(vb.normals), 3)]
        #tVector0 = [vb.texcoords[x:x+tdim0]      for x in range(0, len(vb.texcoords), tdim0)]
        #tVector1 = [vb.texcoords_1[x:x+tdim1]    for x in range(0, len(vb.texcoords_1), tdim1)]
        #cVector  = [vb.diffusecolors[x:x+cDim]   for x in range(0, len(vb.diffusecolors), cDim)]
        self.startVertexbuffer(((len(vb.vertices) != 0)&positions),
                               ((len(vb.normals) != 0)&normals),
                               ((len(vb.texcoords) != 0)&texcoords0),
                               ((len(vb.texcoords_1) != 0)&texcoords1),
                               ((len(vb.diffusecolors) != 0)&diffusecolors),
                               tex0_dimensions = vb.texcoordDimensions[0],
                               tex1_dimensions = vb.texcoordDimensions[1])
        #print "Tex bank 0: ", tdim0, texcoords0, len(tVector0)
        #print "Tex bank 1 ", tdim1, texcoords1, len(tVector1)

        counter = 0
        while counter < len(vb.vertices):
            self.startVertex()
            if positions:
                self.outputPosition(vb.vertices[counter])
            if normals:
                try: self.outputNormal(vb.normals[counter])
                except: pass
            if texcoords0:
                try: self.outputTexcoord(vb.texcoords[counter])
                except: pass
            if texcoords1:
                try: self.outputTexcoord(vb.texcoords_1[counter])
                except: pass
            if diffusecolors:
                try: self.outputDiffuseColor(vb.diffusecolors[counter])
                except: pass
            if tangents:
                try: self.outputTangent(vb.tangents[counter])
                except: pass
            self.endVertex()
            counter += 1
        self.endVertexbuffer()

    #############################################################################
    # OgreXMLExport: File I/O and internals
    #
    def openOutputXML(self, localfile, overwrite):
        if os.path.exists(localfile):
            if overwrite == False:
                print ("OgreXMLExport: ERROR: output file '%s' already exists!\n" % filename)
                return
            else:
                os.unlink(localfile)
        try: self.outputXMLFile = open(localfile, "w")
        except IOError:
            print ("OgreXMLExport: ERROR: Unable to open file '%s' for writing!" % filename)
            self.outputXMLFile = None

    def closeOutputXML(self):
        self.outputXMLFile.close()
        self.outputXMLFile = None

    def __outputXML(self, msg):
        if self.outputXMLFile == None:
            return
        indent_str = ""
        for i in range(self.indent):
            indent_str = indent_str + " "
        self.outputXMLFile.write((indent_str + str(msg) + "\n"))

    def __increaseIndent(self):
        self.indent += 4

    def __decreaseIndent(self):
        if self.indent > 0: self.indent -= 4

    #############################################################################
    # OgreXMLExport: Ogre XML output methods
    #
    def startMesh(self):
        self.__outputXML("<mesh>")
        self.__increaseIndent()

    def endMesh(self):
        self.__decreaseIndent()
        self.__outputXML("</mesh>")

    def startVertexbuffer(self, position=True, normal=True, texcoord0=True, texcoord1=False, diffusecolor=False, tex0_dimensions=2, tex1_dimensions=2):
        s_out = ""
        t_count = 0
        if normal == True:   s_out += "normals=\"true\" "
        if position == True: s_out += "positions=\"true\" "
        if texcoord0 == True:
            s_out += "texture_coord_dimensions_0=\"%d\" " % tex0_dimensions
            t_count += 1
        if texcoord1 == True:
            s_out += "texture_coord_dimensions_1=\"%d\" " % tex1_dimensions
            t_count += 1
        if t_count > 0:
            s_out += ("texture_coords=\"%d\" " % t_count)
        if diffusecolor == True:
            s_out += "colours_diffuse=\"true\" "
        self.__outputXML("<vertexbuffer %s>" % s_out)
        self.__increaseIndent()

    def endVertexbuffer(self):
        self.__decreaseIndent()
        self.__outputXML("</vertexbuffer>")

    def startSharedgeometry(self, vertices):
        self.__outputXML("<sharedgeometry vertexcount=\"%d\">" % vertices)
        self.__increaseIndent()

    def endSharedgeometry(self):
        self.__decreaseIndent()
        self.__outputXML("</sharedgeometry>")

    def startVertex(self):
        self.__outputXML("<vertex>")
        self.__increaseIndent()

    def endVertex(self):
        self.__decreaseIndent()
        self.__outputXML("</vertex>")

    def startSubmeshes(self):
        self.__outputXML("<submeshes>")
        self.__increaseIndent()

    def endSubmeshes(self):
        self.__decreaseIndent()
        self.__outputXML("</submeshes>")

    def startSubmesh(self, material, sharedvertices, longindices=False, operationtype="triangle_list"):
        s_out = "material=\"%s\" " % material
        s_out += "usesharedvertices=\"%s\" " % str(sharedvertices).lower()
        s_out += "use32bitindexes=\"%s\" " % str(longindices).lower()
        s_out += "operationtype=\"%s\"" % operationtype
        self.__outputXML("<submesh %s>" % s_out)
        self.__increaseIndent()

    def endSubmesh(self):
        self.__decreaseIndent()
        self.__outputXML("</submesh>")

    def startFaces(self, faces):
        self.__outputXML("<faces count=\"%d\">" % faces)
        self.__increaseIndent()

    def endFaces(self):
        self.__decreaseIndent()
        self.__outputXML("</faces>")

    def startGeometry(self, vertices):
        self.__outputXML("<geometry vertexcount=\"%d\">" %vertices)
        self.__increaseIndent()

    def endGeometry(self):
        self.__decreaseIndent()
        self.__outputXML("</geometry>")

    def outputPosition(self, v):
        o = "<position "
        keys = [ 'x', 'y', 'z', 'w' ]
        for i in range(len(v)):
            o += '%s="%1.6f" ' % (keys[i], v[i])
        o += '/>'
        self.__outputXML(o)

    def outputNormal(self, v):
        o = "<normal "
        keys = [ 'x', 'y', 'z', 'w' ]
        for i in range(len(v)):
            o += '%s="%1.6f" ' % (keys[i], v[i])
        o += '/>'
        self.__outputXML(o)

    def outputTexcoord(self, t):
        o = "<texcoord "
        keys = [ 'u', 'v', 'w', 'x' ]
        for i in range(len(t)):
            o += '%s="%1.6f" ' % (keys[i], t[i])
        o += '/>'
        self.__outputXML(o)

    def outputFace(self, f):
        o = "<face "
        keys = [ 'v1', 'v2', 'v3' ]
        for i in range(len(f)):
            o += '%s="%d" ' % (keys[i], f[i])
        o += '/>'
        self.__outputXML(o)

    def outputDiffuseColor(self, c):
        o = '<colour_diffuse value="'
        keys = [ 'u', 'v', 'w', 'x' ]
        for i in range(len(c)):
            o += '%1.6f ' % (c[i])
        o += '"/>'
        self.__outputXML(o)

    def outputTangent(self, t):
        o = "<tangent "
        keys = [ 'x', 'y', 'z', 'w' ]
        for i in range(len(t)):
            o += '%s="%1.6f" ' % (keys[i], t[i])
        o += '/>'
        self.__outputXML(o)

#############################################################################
# OgreMeshImport class - for importing binary ogre meshes
#

class OgreMeshImport():
    """ OgreMeshImport() is able to eat binary Ogre mesh input and fill in the provided
        MeshContainer class for further use.
        OgreMeshImport wraps the binary through OgreXMLConverter. If such tool does
        not exist in the system path, the class will fail with IOError
        - This is a harsh hack to handle binary meshes, but it will be sufficient since
          using of the binary meshes is not the priority for this tool.
        - Binary exporter will not be written.
    """
    def __init__(self, meshcontainer):
        self.meshcontainer = meshcontainer

    ###
    # OgreMeshImport I/O
    #
    def fromFile(self, localfile):
        import subprocess
        rc = subprocess.call(["OgreXMLConverter", "-q", localfile, "/tmp/tempogremesh.mesh.xml"])
        if rc != 0: raise IOError
        ml = OgreXMLImport(self.meshcontainer)
        ml.fromFile("/tmp/tempogremesh.mesh.xml")

    def toFile(self, localfile):
        print ("Exporting of binary Ogre mesh not supported")

    ###
    # OgreMeshImport stats, for debugging
    #
    def printStatistics(self):
        self.meshcontainer.printStatistics()

#############################################################################
# VTKMeshImport class - for importing VTK point clouds
#

class VTKMeshImport():
    """ VTKMeshImport() is able to eat ASCII VTK fileformat, which contains point cloud
        data and index offsets into polygons.
    """
    def __init__(self, meshcontainer):
        self.meshcontainer = meshcontainer

    ###
    # OgreMeshImport I/O
    #
    def fromFile(self, localfile):
        """ Import from the .vtk file. exceptions are passed through and expected to be caught
            elsewhere.
        """
        f = open(localfile)
        while True:
            line = f.readline()
            if line.find("POINTS") != -1:
                self.meshcontainer.newSubmesh()
                nPoints = int(line.split()[1])
                for index in range(nPoints):
                    vector = f.readline().split(" ")
                    self.meshcontainer.addVertex([float(vector[0]), float(vector[1]), float(vector[2])])
            if line.find("POLYGONS") != -1:
                nPolygons = int(line.split()[1])
                for index in range(nPolygons):
                    vector = f.readline().split(" ")
                    self.meshcontainer.addFace([int(vector[1]), int(vector[2]), int(vector[3])])
            if line.find("COLOR_SCALARS") != -1:
                # at this point we already know nPoints
                for index in range(nPoints):
                    vector = f.readline().split(" ")
                    self.meshcontainer.addDiffuseColor([float(vector[0]), float(vector[1]), float(vector[2])])
                print ("%d colours read" % nPoints)

            if line == "": break
            #print line.strip()

    ###
    # VTKMeshImport stats, for debugging
    #
    def printStatistics(self):
        self.meshcontainer.printStatistics()

#############################################################################
# OBJMeshImport class - for importing Wavefront OBJ files
#

class OBJMeshImport():
    """ OBJMeshImport() is able to eat ASCII OBJ fileformat, which contains mesh formations
        It is assumed that a single OBJ file contains one object which is split between single
        shared geometry and multiple (>=1) submeshes. Vertices are stored in a single shared geometry
        block, and triangles after that are stored in submeshes, separated by a new material
        defined in the input file.
    """
    def __init__(self, meshcontainer):
        self.meshcontainer = meshcontainer

    ###
    # OBJMeshImport I/O
    #
    def fromFile(self, localfile):
        """ Import from the .obj file. exceptions are passed through and expected to be caught
            elsewhere. Note, normals in this export are ignored, and it is strongly recommended
            that recalculateNormals() is called afterwards, especially if lighting calculations
            are expected to work for the object.
        """
        f = open(localfile)
        #
        # OBJ file format does not store indexed attribute lists. Instead it stores individual
        # vertex attributes, which are referenced by face corners a number of times. This import
        # code will read the attributes, and then reorder the data back into indexed form
        # the reasoning is that after indexing the meshes can be drawn with a single drawcalls using
        # buffered GL objects. likewise, after indexing all MeshContainer class tools are usabe
        # e.g. scaling, or different optimization methods. For example, material collapsin is something
        # that is suggested to be used after this import to optimize the indexed mesh further.
        #

        # Empty placeholdes for the attribute data, collected from the import OBJ
        vertices = []
        texcoords = []
        #
        faceindices = []
        texindices = []
        #
        facelists = []
        texlists = []
        #
        materials = []
        totalVertices = 0
        totalTexcoords = 0

        # This loop reads the raw data from OBJ file. It does not recognize all OBJ tokens yet, only those which
        # are the mandatory ones.
        #
        while True:
            line = f.readline()
            if len(line) == 0: break
            l = line.strip().split(" ")
            l = filter(lambda x: x != "", l)    # Skip whitespaces.
            if len(l) == 0: continue

            if l[0].lower() == "v":
                vertices.append([float(l[1]), float(l[2]), float(l[3])])
                totalVertices += 1
                #print "  Vertex:", len(vertices)-1, vertices[-1]
            if l[0].lower() == "vt":
                texcoords.append([abs(float(l[1])), 1.0-abs(float(l[2]))])
                totalTexcoords += 1
                #print "Texcoord:", len(texcoords)-1, texcoords[-1]
            if l[0].lower() == "vn":
                # Normals are skipped
                pass
            if l[0].lower() == "usemtl":
                if len(faceindices) != 0: facelists.append(faceindices)
                faceindices = []
                if len(texindices) != 0: texlists.append(texindices)
                texindices = []
                materials.append(l[1])
                print ("Submesh material", l[1])
            if l[0] == "f":
                #
                # This is a poor excuse of triangulation. OBJs with triance meshes will be loaded
                # as such, but OBJs with higher order of subsurfaces (e.g. quas) will be triangulated
                # in vertex ascending order inside one face. Because this is not real triangulation
                # there might be conversion errors. A real polygon triangulation should be implemented
                # here..
                #
                corners = l[1:]
                #print corners
                for i in range(len(corners)-2):
                    a = corners[0].split("/")
                    b = corners[1+i].split("/")
                    c = corners[2+i].split("/")
                    t = [ int(a[0])-1, int(b[0])-1, int(c[0])-1 ]
                    #print " faceindex:", t[0], totalVertices, totalVertices + t[0] + 1
                    for gg in [0, 1, 2]:
                        if t[gg] < 0: t[gg] = totalVertices + t[gg] + 1
                    faceindices.append(t)
                    try:
                        t = [ int(a[1])-1, int(b[1])-1, int(c[1])-1 ]
                        for gg in [0, 1, 2]:
                            if t[gg] < 0: t[gg] = totalTexcoords + t[gg] + 1
                        texindices.append(t)
                    except IndexError: pass
                    except ValueError: pass # empty item in the middle

        # flush the final submesh
        facelists.append(faceindices)
        texlists.append(texindices)

        print ("Wavefront OBJ, %d submeshes, %d vertices, %d texcoords RAW" % \
            (len(facelists), len(vertices), len(texcoords)))

        #print len(facelists)                   # Intermediate debug prints
        #print "Vertices", vertices
        #print "Normals ", normals
        #print "texcoord", texcoords
        #print "faceindi", faceindices
        #print "texindic", texindices
        #print "normindi", normindices
        #print "facelist", facelists
        #print "texlists", texlists
        #print "normlist", normlists
        #print "material", materials

        self.meshcontainer.newSharedGeometry()
        d = {}
        index = 0

        #
        # This loop walks through the raw data imported from the OBJ, and recalculates face index
        # lists finally storing them into an array of submeshes. All vertex attributes ace hence
        # stored in a single shared geometry buffer, and all submeshes face indices are sorted into
        # their individual submeshes.
        #
        submeshes = []
        for sm in range(len(facelists)): # loop all submeshes
            faces = []
            for i in range(len(facelists[sm])):
                face = []
                for p in [0, 1, 2]:
                    key = "%s" % (facelists[sm][i][p])
                    try: key += "x%s" % (texlists[sm][i][p])
                    except IndexError: pass
                    try:
                        t = d[key]
                        face.append(t)
                        #print "old:", key
                    except KeyError:
                        d[key] = index
                        #print "new:", key
                        self.meshcontainer.addVertex([vertices[facelists[sm][i][p]][0],
                                                      vertices[facelists[sm][i][p]][1],
                                                      vertices[facelists[sm][i][p]][2]])
                        #print " append vertex:", [vertices[facelists[sm][i][p]][0], vertices[facelists[sm][i][p]][1], vertices[facelists[sm][i][p]][2]]
                        try:
                            self.meshcontainer.addTexcoord([texcoords[texlists[sm][i][p]][0],
                                                            texcoords[texlists[sm][i][p]][1]], 0)
                            #print " append texcoord:", [texcoords[texlists[sm][i][p]][0], texcoords[texlists[sm][i][p]][1]]
                        except IndexError: pass
                        face.append(index)
                        #print face
                        index += 1
                faces.append(face)
                #print faces[-1]
            submeshes.append(faces)
        #print submeshes

        # Finally, sorted indices are written into the MeshContainer
        index = 0
        for sm in submeshes:
            try: mRef = materials[index]
            except IndexError: mRef = ""
            self.meshcontainer.newSubmesh(materialref=mRef)
            for face in sm:
                self.meshcontainer.addFace([face[0], face[1], face[2]])
            index += 1

    ###
    # OBJMeshImport stats, for debugging
    #
    def printStatistics(self):
        self.meshcontainer.printStatistics()

#############################################################################
# NMESHImport class
#

class NMESHImport():
    """ NMESHImport() is able to eat Neocortex native binary mesh data.
    """

    # Nmesh header, attrs:
    HEADER_ATTR_HALFFLOAT       = 0x01
    HEADER_ATTR_SHAREDGEOMETRY  = 0x02
    HEADER_ATTR_VALIDAABB       = 0x04

    # Nmesh header, geomattrs:
    HEADER_ATTR_VERTEX          = 0x01
    HEADER_ATTR_NORMAL          = 0x02
    HEADER_ATTR_TEXCOORD0       = 0x04
    HEADER_ATTR_TEXCOORD1       = 0x08
    HEADER_ATTR_COLOR           = 0x10
    HEADER_ATTR_TANGENT         = 0x20
    HEADER_ATTR_BONEWEIGHT      = 0x40
    HEADER_ATTR_BONEINDEX       = 0x80

    # Possible primitive types:
    HEADER_PRIM_TRIANGLE        = 1
    HEADER_PRIM_LINE            = 2
    HEADER_PRIM_POINT           = 3

    # Possible index lengths:
    HEADER_INDEXTYPE_8BIT       = 1
    HEADER_INDEXTYPE_16BIT      = 2
    HEADER_INDEXTYPE_32BIT      = 3

    def __init__(self, meshcontainer):
        self.meshcontainer = meshcontainer
        self.structs = {
            'NHDR': [ { 'name':'endianess', 'len':4, 'type':'I' },
                      { 'name':'attr',      'len':1, 'type':'B' },
                      { 'name':'geomattr',  'len':1, 'type':'B' },
                      { 'name':'#submesh',  'len':1, 'type':'B' },
                      { 'name':'version',   'len':1, 'type':'B' },
                      { 'name':'aabb xmin', 'len':4, 'type':'f', 'forcetype':type(0.0) },
                      { 'name':'aabb xmax', 'len':4, 'type':'f', 'forcetype':type(0.0) },
                      { 'name':'aabb ymin', 'len':4, 'type':'f', 'forcetype':type(0.0) },
                      { 'name':'aabb ymax', 'len':4, 'type':'f', 'forcetype':type(0.0) },
                      { 'name':'aabb zmin', 'len':4, 'type':'f', 'forcetype':type(0.0) },
                      { 'name':'aabb zmax', 'len':4, 'type':'f', 'forcetype':type(0.0) },
                    ],
            'NSHG': [ { 'name':'sg length', 'len':4, 'type':'I' },
                    ],
            'NMSH': [ { 'name':'indices',   'len':4, 'type':'I' },
                      { 'name':'indextype', 'len':1, 'type':'B' },
                      { 'name':'primtype',  'len':1, 'type':'B' },
                      { 'name':'PAD1',      'len':1, 'type':'B' },
                      { 'name':'PAD2',      'len':1, 'type':'B' },
                      { 'name':'geomlen',   'len':4, 'type':'I' },
                      { 'name':'indexlen',  'len':4, 'type':'I' },
                      { 'name':'matname',   'len':64,'type':'64s', 'forcetype':type("") }
                    ],
            # End of data:
            'FEND': []
        }

        # Decoding status flags:
        self.halffloats     = False
        self.sharedgeometry = False
        self.validaabb      = False

    def __unpackHeader(self, f):
        data = f.read(4)
        if len(data) != 4: return 'FEND'
        d = int(struct.unpack('I', data)[0])
        return '%s%s%s%s' % (chr((d>>24)&255), chr((d>>16)&255), chr((d>>8)&255), chr(d&255))

    def __parseHeader(self, header, f):

        # First, check the header type:
        #
        h = self.structs.get(header, None)
        if h == None:
            print ("Unrecognized header '%s'" % header)
            return False

        if header == 'FEND': return False

        # Second, parse the header fields:
        #
        print ("Parsing header '%s'" % header)
        for field in h:

            # Read next data block:
            data = f.read(field['len'])
            if len(data) != field['len']:
                print ("File end reached")
                break

            if field.get('forcetype', None) == type(0.0):
                d = float(struct.unpack(field['type'], data)[0])
            elif field.get('forcetype', None) == type(""):
                d = str(struct.unpack(field['type'], data)[0].decode('utf-8').strip())
            else:
                d = int(struct.unpack(field['type'], data)[0])

            try: self.__parseField(field['name'], d, f)
            except Exception as e:
                print ("%s" % repr(e))

        return True

    def __parseField(self, name, d, f):
        print (" %s: %s" % (name, d))

        if name == 'endianess': # Endianess check:
            if d != 0x01020304:
                print ("Machine native endianess does not match with file endianess!")

        if name == 'attr': # header attributes:
            self.halffloats     = ((d&self.HEADER_ATTR_HALFFLOAT)!=0)
            self.sharedgeometry = ((d&self.HEADER_ATTR_SHAREDGEOMETRY)!=0)
            self.validaabb      = ((d&self.HEADER_ATTR_VALIDAABB)!=0)
            print ("  HEADER_ATTR_HALFFLOAT      = %s" % self.halffloats)
            print ("  HEADER_ATTR_SHAREDGEOMETRY = %s" % self.sharedgeometry)
            print ("  HEADER_ATTR_VALIDAABB      = %s" % self.validaabb)

        if name == 'geomattr': # header geometry attributes:
            self.vertices       = ((d&self.HEADER_ATTR_VERTEX)!=0)
            self.normals        = ((d&self.HEADER_ATTR_NORMAL)!=0)
            self.texcoords0     = ((d&self.HEADER_ATTR_TEXCOORD0)!=0)
            self.texcoords1     = ((d&self.HEADER_ATTR_TEXCOORD1)!=0)
            self.colors         = ((d&self.HEADER_ATTR_COLOR)!=0)
            self.tangents       = ((d&self.HEADER_ATTR_TANGENT)!=0)
            self.boneweights    = ((d&self.HEADER_ATTR_BONEWEIGHT)!=0)
            self.boneindices    = ((d&self.HEADER_ATTR_BONEINDEX)!=0)
            print ("  HEADER_ATTR_VERTEX         = %s" % self.vertices)
            print ("  HEADER_ATTR_NORMAL         = %s" % self.normals)
            print ("  HEADER_ATTR_TEXCOORD0      = %s" % self.texcoords0)
            print ("  HEADER_ATTR_TEXCOORD1      = %s" % self.texcoords1)
            print ("  HEADER_ATTR_COLOR          = %s" % self.colors)
            print ("  HEADER_ATTR_TANGENT        = %s" % self.tangents)
            print ("  HEADER_ATTR_BONEWEIGHT     = %s" % self.boneweights)
            print ("  HEADER_ATTR_BONEINDEX      = %s" % self.boneindices)

        if name == 'sg length': # Shared geometry header:
            self.sharedgeometrylength = int(d)

            # This is the last fielf of NSHG, next read the RAW data:
            self.meshcontainer.newSharedGeometry()

            sgData = f.read(self.sharedgeometrylength)
            if self.halffloats == True:
                sgData = numpy.frombuffer(sgData, dtype=numpy.float16).tolist()
            else:
                sgData = numpy.frombuffer(sgData, dtype=numpy.float32).tolist()
            self.__decodeGeometryBuffer(sgData)

        if name == 'indices':
            self.indices = int(d)

        if name == 'indextype':
            _m = { self.HEADER_INDEXTYPE_8BIT: 1, self.HEADER_INDEXTYPE_16BIT: 2, self.HEADER_INDEXTYPE_32BIT: 4 }
            self.indexsize = _m.get(int(d))
            print ("  index size %d bytes" % self.indexsize)

        if name == 'primtype':
            _m = { self.HEADER_PRIM_POINT:"POINTS", self.HEADER_PRIM_LINE:"LINES", self.HEADER_PRIM_TRIANGLE:"TRIANGLES"}
            self.primtype = int(d)
            print ("  Primitive type %s" % _m.get(self.primtype))

        if name == 'geomlen':
            self.meshgeometrylength = int(d)

        if name == 'indexlen':
            self.meshindexlength = int(d)

        if name == 'matname':
            self.matname = str(d.split('\0',1)[0])

            # Create new submesh for the data
            self.meshcontainer.newSubmesh(materialref=self.matname)

            # Decode the actual data:
            if self.meshgeometrylength > 0:
                m_gdata = f.read(self.meshgeometrylength)
                if self.halffloats == True:
                    m_gdata = numpy.frombuffer(m_gdata, dtype=numpy.float16).tolist()
                else:
                    m_gdata = numpy.frombuffer(m_gdata, dtype=numpy.float32).tolist()
                self.__decodeGeometryBuffer(m_gdata)

            m_idata = f.read(self.meshindexlength)
            if self.indexsize == 1:
                m_idata = numpy.frombuffer(m_idata, dtype=numpy.uint8)
            elif self.indexsize == 2:
                m_idata = numpy.frombuffer(m_idata, dtype=numpy.uint16)
            else:
                m_idata = numpy.frombuffer(m_idata, dtype=numpy.uint32)
            self.__decodeIndexBuffer(m_idata)

    def __decodeGeometryBuffer(self, b):
        print ("len geom data = %d" % len(b))
        _d = [ { 'name': 'vertex',     'decode': self.vertices,    'elements': 4 },
               { 'name': 'normal',     'decode': self.normals,     'elements': 3 + int(self.halffloats==1) },
               { 'name': 'texcoord0',  'decode': self.texcoords0,  'elements': 2 },
               { 'name': 'texcoord1',  'decode': self.texcoords1,  'elements': 2 },
               { 'name': 'color',      'decode': self.colors,      'elements': 4 },
               { 'name': 'tangent',    'decode': self.tangents,    'elements': 4 },
               { 'name': 'boneweight', 'decode': self.boneweights, 'elements': 4 },
               { 'name': 'boneindex',  'decode': self.boneindices, 'elements': 4 }
             ]

        i = 0
        while i < len(b):
            for dtype in _d:
                if dtype['decode'] == False: continue
                data = b[i:i+dtype['elements']]
                #print ("parsing '%s' %s" % (dtype['name'], data))
                if dtype['name'] == 'vertex':     self.meshcontainer.addVertex(data)
                if dtype['name'] == 'normal':     self.meshcontainer.addNormal(data[:3])
                if dtype['name'] == 'texcoord0':  self.meshcontainer.addTexcoord(data, 0)
                if dtype['name'] == 'texcoord1':  self.meshcontainer.addTexcoord(data, 1)
                if dtype['name'] == 'color':      self.meshcontainer.addDiffuseColor(data)
                if dtype['name'] == 'tangent':    self.meshcontainer.addTangent(data)
                if dtype['name'] == 'boneweight': pass #self.meshcontainer.addBoneWeight(data)
                if dtype['name'] == 'boneindex':  pass #self.meshcontainer.addBoneIndex(data)
                i += dtype['elements']

    def __decodeIndexBuffer(self, b):
        #print("%s" % b)
        i = 0
        while i < len(b):
            data = b[i:i+3]
            self.meshcontainer.addFace(data)
            i += 3

    ###
    # NMESHImport: I/O
    #
    def fromFile(self, localfile):
        with open(localfile, "rb") as f:
            result = True
            while result == True:
                h = self.__unpackHeader(f)
                result = self.__parseHeader(h, f)
        print ("Done!")


    ###
    # NMESHImport: debug
    #
    def printStatistics(self):
        self.meshcontainer.printStatistics()

#############################################################################
# NMESHExport: Neocortex mesh exporter
#
class NMESHExport():
    """ class NMESHExport(): I/O component for writing formatted Neocortex mesh notation
    """

    # Nmesh header, attrs:
    HEADER_ATTR_HALFFLOAT       = 0x01
    HEADER_ATTR_SHAREDGEOMETRY  = 0x02
    HEADER_ATTR_VALIDAABB       = 0x04

    # Nmesh header, geomattrs:
    HEADER_ATTR_VERTEX          = 0x01
    HEADER_ATTR_NORMAL          = 0x02
    HEADER_ATTR_TEXCOORD0       = 0x04
    HEADER_ATTR_TEXCOORD1       = 0x08
    HEADER_ATTR_COLOR           = 0x10
    HEADER_ATTR_TANGENT         = 0x20
    HEADER_ATTR_BONEWEIGHT      = 0x40
    HEADER_ATTR_BONEINDEX       = 0x80

    # Possible index lengths:
    HEADER_INDEXTYPE_8BIT       = 1
    HEADER_INDEXTYPE_16BIT      = 2
    HEADER_INDEXTYPE_32BIT      = 3

    # Possible primitive types:
    HEADER_PRIM_TRIANGLE        = 1
    HEADER_PRIM_LINE            = 2
    HEADER_PRIM_POINT           = 3

    def __init__(self, meshcontainer, localfile, overwrite=False):
        self.meshcontainer = meshcontainer
        self.outputFile = None
        self.__openOutput(localfile, overwrite)

    def __openOutput(self, localfile, overwrite):
        if os.path.exists(localfile):
            if overwrite == False:
                print ("NMESHExport: ERROR: output file '%s' already exists!\n" % filename)
                return
            else: os.unlink(localfile)
        try: self.outputFile = open(localfile, "wb")
        except IOError:
            print ("NMESHExport: ERROR: Unable to open file '%s' for writing!" % filename)
            self.outputFile = None

    def toFile(self, localfile, positions=True, normals=True, texcoords0=True, texcoords1=True, diffusecolors=True, tangents=True):

        if self.meshcontainer.sharedgeometry != None:
            vb = self.meshcontainer.sharedgeometry
        else:
            vb = self.meshcontainer.submeshes[0].vertexBuffer

        # Write Nmesh header:
        attr = self.HEADER_ATTR_HALFFLOAT + \
               self.HEADER_ATTR_SHAREDGEOMETRY * int(self.meshcontainer.sharedgeometry != None)
               #self.HEADER_ATTR_VALIDAABB

        geomattr = self.HEADER_ATTR_VERTEX * int(positions == True and len(vb.vertices)>0) + \
                   self.HEADER_ATTR_NORMAL * int(normals == True and len(vb.normals)>0) + \
                   self.HEADER_ATTR_TEXCOORD0 * int(texcoords0 == True and len(vb.texcoords)>0) + \
                   self.HEADER_ATTR_TEXCOORD1 * int(texcoords1 == True and len(vb.texcoords_1)>0) + \
                   self.HEADER_ATTR_COLOR * int(diffusecolors == True and len(vb.diffusecolors)>0) + \
                   self.HEADER_ATTR_TANGENT * int(tangents == True and len(vb.tangents)>0)
                   #self.HEADER_ATTR_VERTEX * int(positions == True) + \
                   #self.HEADER_ATTR_VERTEX * int(positions == True)

        d = struct.pack('IIBBBBffffff', ((ord('N')<<24) + (ord('H')<<16) + (ord('D')<<8) + (ord('R'))),
                                     0x01020304,
                                     attr,
                                     geomattr,
                                     len(self.meshcontainer.submeshes),
                                     2,
                                     0.0,0.0,0.0,0.0,0.0,0.0)
        self.outputFile.write(d)

        if self.meshcontainer.sharedgeometry != None:
            b = self.__buildGeometryBuffer(self.meshcontainer.sharedgeometry)
            d = struct.pack('II', ((ord('N')<<24) + (ord('S')<<16) + (ord('H')<<8) + (ord('G'))),
                                  len(b))
            self.outputFile.write(d)
            self.outputFile.write(b)

        for m in self.meshcontainer.submeshes:

            if self.meshcontainer.sharedgeometry != None:
                vb = self.meshcontainer.sharedgeometry
            else:
                vb = m.vertexBuffer

            if len(vb.vertices) < 256:     iType = numpy.uint8;  _i=self.HEADER_INDEXTYPE_8BIT
            elif len(vb.vertices) < 65556: iType = numpy.uint16; _i=self.HEADER_INDEXTYPE_16BIT
            else:                          iType = numpy.uint32; _i=self.HEADER_INDEXTYPE_32BIT

            iBuf = numpy.array([], dtype=iType).tobytes()
            for f in m.faces:
                t = numpy.array(f, dtype=iType)
                iBuf += t.tobytes()

            gBuf = b''
            if (len(m.vertexBuffer.vertices) > 0):
                gBuf = self.__buildGeometryBuffer(m.vertexBuffer)

            a = m.materialref
            d = struct.pack('IIBBBBII64s', ((ord('N')<<24) + (ord('M')<<16) + (ord('S')<<8) + (ord('H'))),
                                           3*len(m.faces),
                                           _i,
                                           self.HEADER_PRIM_TRIANGLE,
                                           0,
                                           0,
                                           len(gBuf),
                                           len(iBuf),
                                           a.encode('utf-8') + numpy.array((64-len(a))*[0], dtype=numpy.uint8).tobytes())
            self.outputFile.write(d)
            if len(gBuf) != 0: self.outputFile.write(gBuf)
            self.outputFile.write(iBuf)

    def __buildGeometryBuffer(self, vb, positions=True, normals=True, texcoords0=True, texcoords1=True, diffusecolors=True, tangents=True):
        _d = [ { 'name': 'vertex',     'encode': (len(vb.vertices)>0 and positions),          'ptr': vb.vertices },
               { 'name': 'normal',     'encode': (len(vb.normals)>0 and normals),             'ptr': vb.normals },
               { 'name': 'texcoord0',  'encode': (len(vb.texcoords)>0 and texcoords0),        'ptr': vb.texcoords },
               { 'name': 'texcoord1',  'encode': (len(vb.texcoords_1)>0 and texcoords1),      'ptr': vb.texcoords_1 },
               { 'name': 'color',      'encode': (len(vb.diffusecolors)>0 and diffusecolors), 'ptr': vb.diffusecolors },
               { 'name': 'tangent',    'encode': (len(vb.tangents)>0 and tangents),           'ptr': vb.tangents },
               #{ 'name': 'boneweight', 'encode': len(vb.vertices)>0,      'elements': 4 },
               #{ 'name': 'boneindex',  'encode': len(vb.vertices)>0,      'elements': 4 }
             ]

        total = 0
        b = numpy.array([], dtype=numpy.float16).tobytes()
        for i in range(len(vb.vertices)):
            for dtype in _d:
                if dtype['encode'] == False: continue
                data = [ a for a in dtype['ptr'][i] ]
                if dtype['name'] == 'vertex' and len(data) == 3: data.append(0.0)
                if dtype['name'] == 'normal': data.append(0.0)
                #print ("encoding %s %s" % (dtype['name'], data))
                b += numpy.array(data, dtype=numpy.float16).tobytes()
                #if dtype['name'] == 'normals': b += numpy.array([0.0], dtype=numpy.float16).tobytes()
                total += len(data)
        #print ("geombuf %d elements %d bytes" % (total, len(b)))
        return b

#############################################################################
# OBJExport: Wavefront OBJ mesh exporter
#
class OBJExport():
    """ class OBJExport(): I/O component for writing formatted Wavefront OBJ data
    """

    def __init__(self, meshcontainer, localfile, overwrite=False):
        self.meshcontainer = meshcontainer
        self.outputFile = None
        self.__openOutput(localfile, overwrite)

    def __openOutput(self, localfile, overwrite):
        if os.path.exists(localfile):
            if overwrite == False:
                print ("OBJExport: ERROR: output file '%s' already exists!\n" % filename)
                return
            else: os.unlink(localfile)
        try: self.outputFile = open(localfile, "w")
        except IOError:
            print ("OBJExport: ERROR: Unable to open file '%s' for writing!" % filename)
            self.outputFile = None

    def __writeOutput(self, s):
        if self.outputFile != None:
            self.outputFile.write(s + '\n')

    def toFile(self, localfile, positions=True, normals=True, texcoords0=True, texcoords1=True, diffusecolors=True, tangents=True):


        if self.meshcontainer.sharedgeometry != None:
            print ("OBJExport: does not support exporting sharedgeometry meshes yet. Abort!")
            return

        i = 0
        for m in self.meshcontainer.submeshes:
            self.__writeOutput("o Export%d" % i)
            for v in m.vertexBuffer.vertices:
                self.__writeOutput("v %f %f %f" % (v[0], v[1], v[2]))
            for v in m.vertexBuffer.normals:
                self.__writeOutput("vn %f %f %f" % (v[0], v[1], v[2]))
            for v in m.vertexBuffer.texcoords:
                self.__writeOutput("vt %f %f" % (v[0], v[1]))
            for v in m.faces:
                self.__writeOutput("f %d %d %d" % (v[0]+1, v[1]+1, v[2]+1))
            i += 1

        return


        # Write Nmesh header:
        attr = self.HEADER_ATTR_HALFFLOAT + \
               self.HEADER_ATTR_SHAREDGEOMETRY * int(self.meshcontainer.sharedgeometry != None)
               #self.HEADER_ATTR_VALIDAABB

        geomattr = self.HEADER_ATTR_VERTEX * int(positions == True and len(vb.vertices)>0) + \
                   self.HEADER_ATTR_NORMAL * int(normals == True and len(vb.normals)>0) + \
                   self.HEADER_ATTR_TEXCOORD0 * int(texcoords0 == True and len(vb.texcoords)>0) + \
                   self.HEADER_ATTR_TEXCOORD1 * int(texcoords1 == True and len(vb.texcoords_1)>0) + \
                   self.HEADER_ATTR_COLOR * int(diffusecolors == True and len(vb.diffusecolors)>0) + \
                   self.HEADER_ATTR_TANGENT * int(tangents == True and len(vb.tangents)>0)
                   #self.HEADER_ATTR_VERTEX * int(positions == True) + \
                   #self.HEADER_ATTR_VERTEX * int(positions == True)

        d = struct.pack('IIBBBBffffff', ((ord('N')<<24) + (ord('H')<<16) + (ord('D')<<8) + (ord('R'))),
                                     0x01020304,
                                     attr,
                                     geomattr,
                                     len(self.meshcontainer.submeshes),
                                     2,
                                     0.0,0.0,0.0,0.0,0.0,0.0)
        self.outputFile.write(d)

        if self.meshcontainer.sharedgeometry != None:
            b = self.__buildGeometryBuffer(self.meshcontainer.sharedgeometry)
            d = struct.pack('II', ((ord('N')<<24) + (ord('S')<<16) + (ord('H')<<8) + (ord('G'))),
                                  len(b))
            self.outputFile.write(d)
            self.outputFile.write(b)

        for m in self.meshcontainer.submeshes:

            if self.meshcontainer.sharedgeometry != None:
                vb = self.meshcontainer.sharedgeometry
            else:
                vb = m.vertexBuffer

            if len(vb.vertices) < 256:     iType = numpy.uint8;  _i=self.HEADER_INDEXTYPE_8BIT
            elif len(vb.vertices) < 65556: iType = numpy.uint16; _i=self.HEADER_INDEXTYPE_16BIT
            else:                          iType = numpy.uint32; _i=self.HEADER_INDEXTYPE_32BIT

            iBuf = numpy.array([], dtype=iType).tobytes()
            for f in m.faces:
                t = numpy.array(f, dtype=iType)
                iBuf += t.tobytes()

            gBuf = b''
            if (len(m.vertexBuffer.vertices) > 0):
                gBuf = self.__buildGeometryBuffer(m.vertexBuffer)

            a = m.materialref
            d = struct.pack('IIBBBBII64s', ((ord('N')<<24) + (ord('M')<<16) + (ord('S')<<8) + (ord('H'))),
                                           3*len(m.faces),
                                           _i,
                                           self.HEADER_PRIM_TRIANGLE,
                                           0,
                                           0,
                                           len(gBuf),
                                           len(iBuf),
                                           a.encode('utf-8') + numpy.array((64-len(a))*[0], dtype=numpy.uint8).tobytes())
            self.outputFile.write(d)
            if len(gBuf) != 0: self.outputFile.write(gBuf)
            self.outputFile.write(iBuf)

    def __buildGeometryBuffer(self, vb, positions=True, normals=True, texcoords0=True, texcoords1=True, diffusecolors=True, tangents=True):
        _d = [ { 'name': 'vertex',     'encode': (len(vb.vertices)>0 and positions),          'ptr': vb.vertices },
               { 'name': 'normal',     'encode': (len(vb.normals)>0 and normals),             'ptr': vb.normals },
               { 'name': 'texcoord0',  'encode': (len(vb.texcoords)>0 and texcoords0),        'ptr': vb.texcoords },
               { 'name': 'texcoord1',  'encode': (len(vb.texcoords_1)>0 and texcoords1),      'ptr': vb.texcoords_1 },
               { 'name': 'color',      'encode': (len(vb.diffusecolors)>0 and diffusecolors), 'ptr': vb.diffusecolors },
               { 'name': 'tangent',    'encode': (len(vb.tangents)>0 and tangents),           'ptr': vb.tangents },
               #{ 'name': 'boneweight', 'encode': len(vb.vertices)>0,      'elements': 4 },
               #{ 'name': 'boneindex',  'encode': len(vb.vertices)>0,      'elements': 4 }
             ]

        total = 0
        b = numpy.array([], dtype=numpy.float16).tobytes()
        for i in range(len(vb.vertices)):
            for dtype in _d:
                if dtype['encode'] == False: continue
                data = [ a for a in dtype['ptr'][i] ]
                if dtype['name'] == 'vertex' and len(data) == 3: data.append(0.0)
                if dtype['name'] == 'normal': data.append(0.0)
                #print ("encoding %s %s" % (dtype['name'], data))
                b += numpy.array(data, dtype=numpy.float16).tobytes()
                #if dtype['name'] == 'normals': b += numpy.array([0.0], dtype=numpy.float16).tobytes()
                total += len(data)
        #print ("geombuf %d elements %d bytes" % (total, len(b)))
        return b

###############################################################################
# Unit test case
# - the test case implements input from any supported mesh container and
#   output to any supported export format

if __name__ == "__main__":
    import getopt

    def usage():
        print ("MeshIO -i <input> [-o <outputfile>]")

    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:o:",
            ["input=", "output="])
    except getopt.GetoptError as err:
        usage()
        sys.exit(0)

    input = None
    output = None
    for o, a in opts:
        if o in ("-i", "--input"):
            input = str(a)
        if o in ("-o", "--output"):
            output = str(a)

    if input == None:
        print ("No input given. Abort!")
        usage()
        sys.exit(0)

    try:
        mesh = MeshContainer.MeshContainer()
        meshio = MeshIO(mesh)
        meshio.fromFile(input, None)
        mesh.enableVerbose()
        mesh.printStatistics()
        #mesh.buildAABBMesh()
        if output != None:
            print ("Trying output write to %s" % output)
            #meshio.build3DTextures(prefix="3dtex%03d.bin", overwrite=True)
            meshio.toFile(output, overwrite=True)
    except IOError:
        print ("Error parsing the input file!")
    except OSError:
        print ("OSError, check OgreXMLConverter installation?")
