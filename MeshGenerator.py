#!/usr/bin/env python3
#
# Author: Jarkko Vatjus-Anttila <jvatjusanttila@gmail.com>
#
# For conditions of distribution and use, see copyright notice in license.txt
#

import sys, os, io
import random
import math

import MeshContainer
import MeshIO

class MeshGenerator():
    """ class MeshGenerator(): A collection of procedural methods for
        creating mesh files with various content.
    """

    def __init__(self, meshcontainer, sharedgeometry=False):
        """ MeshGenerator.__init__(meshcontainer, sharedgeometry):
        """
        self.meshcontainer = meshcontainer
        self.sharedgeometry = sharedgeometry

#############################################################################
# Procedural primitive creators
#
# - Plane
# - Cube
# - Cylinder
# - Sphere
#############################################################################

    #########################################################################
    # Plane
    # - Creates a planar mesh
    #
    def createPlane(self, LOD=1, materialref=""):
        """ MeshGenerator.createPlane(self, LOD): Create a plane mesh component
            - Method will create a plane mesh component in XZ-plane whose size is 1 times 1 units in
              coordinate system and which is centered to origin.
        """
        self.meshcontainer.initialize()
        if self.sharedgeometry == True:
            self.meshcontainer.newSharedGeometry()
        else:
            self.meshcontainer.newSubmesh(materialref=materialref)     # The plane is pushed into single submesh

        x_delta = 1.0 / LOD
        z_delta = 1.0 / LOD
        #
        # First we create vertices, normals and texcoords
        #
        for x in range(LOD+1):
            for z in range(LOD+1):
                self.meshcontainer.addVertex([-0.5 + x*x_delta, 0.0, -0.5 + z*z_delta])
                self.meshcontainer.addTexcoord([x*x_delta, z*z_delta], 0)
        #
        # And according to above, we create the faces
        #
        if self.sharedgeometry == True:
            self.meshcontainer.newSubmesh(materialref=materialref)
        for x in range(LOD):
            for z in range(LOD):
                self.meshcontainer.addFace([int(z+x*(LOD+1)), int(LOD+2+z+x*(LOD+1)), int(1+z+x*(LOD+1))])
                self.meshcontainer.addFace([int(z+x*(LOD+1)), int(LOD+1+z+x*(LOD+1)), int(LOD+2+z+x*(LOD+1))])

        self.meshcontainer.recalculateNormals()

    #########################################################################
    # Cube
    # - Constructs a cubic mesh from 6 planar meshes. Does the merge with
    #   meshcontainer provided translation tools
    #
    def createCube(self, LOD=1, materialref=""):
        self.meshcontainer.initialize()
        #self.meshcontainer.newSubmesh()
        if self.sharedgeometry == True:
            self.meshcontainer.newSharedGeometry()
        else:
            self.meshcontainer.newSubmesh(materialref=materialref)     # The plane is pushed into single submesh

        #
        # Temporary mesh container for plane mesh (single side of a cube)
        #
        mesh = MeshContainer.MeshContainer()
        meshgen = MeshGenerator(mesh, sharedgeometry=self.sharedgeometry)
        meshgen.createPlane(LOD)
        # Cube Face 1
        mesh.translate(0.0, -0.5, 0.0)
        self.meshcontainer.merge(mesh)
        # Cube Face 2
        mesh.translate(0.0, 0.5, 0.0)
        mesh.rotate(180, 1, 0, 0)
        mesh.translate(0.0, 0.5, 0.0)
        self.meshcontainer.merge(mesh)
        # Cube Face 3
        mesh.translate(0.0, -0.5, 0.0)
        mesh.rotate(90, 1, 0, 0)
        mesh.translate(0.0, 0.0, 0.5)
        self.meshcontainer.merge(mesh)
        # Cube Face 4
        mesh.translate(0.0, 0.0, -0.5)
        mesh.rotate(90, 0, 1, 0)
        mesh.translate(0.5, 0.0, 0.0)
        self.meshcontainer.merge(mesh)
        # Cube Face 5
        mesh.translate(-0.5,  0.0, 0.0)
        mesh.rotate(90, 0, 1, 0)
        mesh.translate(0.0, 0.0, -0.5)
        self.meshcontainer.merge(mesh)
        # Cube Face 6
        mesh.translate(0.0, 0.0,  0.5)
        mesh.rotate(90, 0, 1, 0)
        mesh.translate(-0.5, 0.0, 0.0)
        self.meshcontainer.merge(mesh)
        #
        self.meshcontainer.collapseSimilars()
        self.meshcontainer.removeDeadFaces()
        self.meshcontainer.recalculateNormals()

    #########################################################################
    # Cylinder
    # - Constructs a cylinder with experimental support for callbacks
    #
    def createCylinder(self, slices=1, LOD=1, minH=0.0, maxH=1.0, callback=None, callbackdata=None, materialref="", r2=0.0):

        nR = 5+LOD
        rDelta = 2.0*math.pi/nR
        hDelta = (maxH-minH)/(slices-1)
        print ("MeshGenerator::createCylinder slices=%d, nR=%d, rDelta=%f, hDelta=%f" % (slices, nR, rDelta, hDelta))

        self.meshcontainer.initialize()
        if self.sharedgeometry == True:
            self.meshcontainer.newSharedGeometry()
        else:
            self.meshcontainer.newSubmesh(materialref=materialref)     # The cylinder is pushed into single submesh

        for i in range(slices):                # Vertical slices
            for j in range(nR):                # Circular slices
                try: r = callback(x=i*hDelta, y=j*rDelta, data=callbackdata)
                except Exception: r = 0.5
                self.meshcontainer.addVertex([r*math.sin(j*rDelta), minH+i*hDelta, r*math.cos(j*rDelta)])
                self.meshcontainer.addTexcoord([j*1.0/(nR-1), i*hDelta], 0)

        # Add cylinder end vertices:
        for i in [0, slices-1]: # ends only
            for j in range(nR):
                try: r = callback(x=i*hDelta, y=j*rDelta, data=callbackdata)
                except Exception: r = 0.5
                self.meshcontainer.addVertex([r2*math.sin(j*rDelta), minH+i*hDelta, r2*math.cos(j*rDelta)])
                self.meshcontainer.addTexcoord([0.0, 0.0], 0)

        if self.sharedgeometry == True:
            self.meshcontainer.newSubmesh()

        for i in range(slices-1):
            for j in range(nR-1):
                self.meshcontainer.addFace([i*nR+j, (i+1)*nR+j+1, (i+1)*nR+j])
                self.meshcontainer.addFace([i*nR+j, i*nR+j+1, (i+1)*nR+j+1])
            self.meshcontainer.addFace([i*nR+j+1, (i+1)*nR+0, (i+1)*nR+j+1])
            self.meshcontainer.addFace([i*nR+j+1, i*nR+0, (i+1)*nR+0])

        sv = slices*nR
        print ("Start vertex %d" % sv)

        # and cylinder end faces:
        lap = 0
        for i in [0, slices-1]:
            for j in range(nR-1):
                self.meshcontainer.addFace([i*nR+j, lap*nR+sv+j+1, lap*nR+sv+j])
                self.meshcontainer.addFace([i*nR+j, i*nR+j+1, lap*nR+sv+j+1])
            self.meshcontainer.addFace([i*nR+j+1, lap*nR+sv+0, lap*nR+sv+j+1])
            self.meshcontainer.addFace([i*nR+j+1, i*nR+0, lap*nR+sv+0])
            lap += 1

        if r2 > 0.0:
            for j in range(nR-1):
                self.meshcontainer.addFace([sv+j, sv+nR+j, sv+nR+j+1])
                self.meshcontainer.addFace([sv+j, sv+nR+j+1, sv+j+1])
            self.meshcontainer.addFace([sv+nR-1, sv+2*nR-1, sv+nR])
            self.meshcontainer.addFace([sv+nR-1, sv+nR, sv])

        # Finally, recalculate normals
        self.meshcontainer.recalculateNormals()

    #########################################################################
    # Sphere
    # - Constructs a Sphere primitive
    # - center at origin, radius 1.0
    #
    def createSphere(self, LOD=1, materialref=""):

        nR = 5+LOD
        slices = LOD+1
        hDelta = 2.0/slices

        self.meshcontainer.initialize()
        if self.sharedgeometry == True:
            self.meshcontainer.newSharedGeometry()
        else:
            self.meshcontainer.newSubmesh(materialref=materialref)     # The sphere is pushed into single submesh

        for i in range(slices+1):          # Vertical slices
            r = math.sqrt(1.0-(-1.0+i*hDelta)*(-1.0+i*hDelta))
            for j in range(nR):            # Circular slices
                a = j*2*math.pi/(nR-1)      # Current circle angle
                self.meshcontainer.addVertex([r*math.sin(a), -1.0+i*hDelta, r*math.cos(a)])
                self.meshcontainer.addNormal([0.0, -1.0, 0.0]) # This is bogus
                self.meshcontainer.addTexcoord([j*1.0/(nR-1), i*hDelta], 0)

        if self.sharedgeometry == True:
            self.meshcontainer.newSubmesh()

        for i in range(slices):
            for j in range(nR-1):
                delta = 0
                if j == nR-2: delta = j+1
                if i == 0:
                    self.meshcontainer.addFace([0, (i+1)*nR+j+1-delta, (i+1)*nR+j])
                    continue
                if i == slices-1:
                    self.meshcontainer.addFace([i*nR+j, i*nR+j+1-delta, slices*nR+1])
                    continue
                if j == nR-2:
                    self.meshcontainer.addFace([i*nR+j, (i+1)*nR, (i+1)*nR+j   ])
                    self.meshcontainer.addFace([i*nR+j, i*nR,     (i+1)*nR ])
                    continue
                self.meshcontainer.addFace([i*nR+j, (i+1)*nR+j+1, (i+1)*nR+j   ])
                self.meshcontainer.addFace([i*nR+j, i*nR+j+1,     (i+1)*nR+j+1 ])

        # These are called for sphericalize to make sure the sphere is in order
        self.meshcontainer.sphericalize()
        self.meshcontainer.recalculateNormals()

#############################################################################

if __name__ == "__main__": # if run standalone

    if 1:
        # - Unit test loop runs a set of test combinations:
        # -  1) sharedgeometry true/false
        # -  2) plane, cobe, sphere, cylinder -types
        # -  3) LOD detail levels 1, 5 and 10

        mesh = MeshContainer.MeshContainer()
        mesh.enableVerbose()

        for LODlevel in [ 1, 5, 10 ]:

            for sharedG in [ False, True ]:

                meshgen = MeshGenerator(mesh, sharedgeometry=sharedG)

                for geomFunc in [ meshgen.createPlane, meshgen.createCube, meshgen.createSphere, meshgen.createCylinder ]:

                    mesh.initialize()
                    geomFunc(LOD=LODlevel)
                    mesh.printStatistics()

        print ("Done!")
        sys.exit(0)

    if 0:
        #meshgen.createCube(LOD=1)
        #meshgen.createCylinder(0.25, 0.75, LOD=10, end1=True, end2=True)
        #meshgen.createSphere(LOD=15)
        meshio = MeshIO.MeshIO(mesh)
        meshio.toFile("./Plane_4.mesh.xml", overwrite=True)
        meshgen.createPlane(LOD=9)
        meshio.toFile("./Plane_100.mesh.xml", overwrite=True)
        meshgen.createPlane(LOD=31)
        meshio.toFile("./Plane_1000.mesh.xml", overwrite=True)
        meshgen.createPlane(LOD=99)
        meshio.toFile("./Plane_10000.mesh.xml", overwrite=True)
        meshgen.createPlane(LOD=316)
        meshio.toFile("./Plane_100000.mesh.xml", overwrite=True)

