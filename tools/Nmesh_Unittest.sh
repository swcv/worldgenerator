for a in "plane" "cube" "cylinder" "sphere"; do
  for b in 1 2 3 4 5; do
    ./tools/MeshTool_Generate.py -t $a -o m1.nmesh -l $b
    ./tools/MeshTool_Info.py -i m1.nmesh -o m2.nmesh
    diff m1.nmesh m2.nmesh
    if [ ! 0 -eq $? ]; then
        echo "ERROR"
        exit 2
    fi
    echo $?
  done
done

