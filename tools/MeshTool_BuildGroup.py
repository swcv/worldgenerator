#!/usr/bin/env python3
#
# Dungeon
#
# Author: Jarkko Vatjus-Anttila <jvatjusanttila@gmail.com>
#
# For conditions of distribution and use, see copyright notice in license.txt
#

###############################################################################

import MeshContainer
import MeshIO
import MeshGenerator

from random import *

def injectInstance(Block, Slice, x, y, z):
    Slice.translate(x, y, z)
    Block.merge(Slice, append=True)
    Slice.translate(-x, -y, -z)

def generateGroup(input, output, number, radius, circle):
    base = MeshContainer.MeshContainer()
    #base.enableVerbose()
    baseIO = MeshIO.MeshIO(base)
    root = MeshContainer.MeshContainer()
    rootIO = MeshIO.MeshIO(root)
    rootIO.fromFile(input, "")
    root.toSharedgeometry()
    root.rotate(180, 1, 0, 0)
    root.printStatistics()
    #
    base.initialize()
    base.toSharedgeometry()
    for m in range(number):
        x = radius * random() - radius/2.0
        y = 0.0
        z = radius * random() - radius/2.0
        injectInstance(base, root, x, y, z)
    base.collapseSimilars()
    baseIO.toFile(output, overwrite=True)

if __name__ == "__main__":
    import getopt, sys, os

    def usage():
        print ("MeshTool_BuildGroup.py -o|--output <file> -i|--input <basemesh> -n|--number <amount> -r|--radius <size> -c|--circle")

    try:
        opts, args = getopt.getopt(sys.argv[1:], "o:i:n:r:c",
            ["output=",  "input=", "number=", "radius=", "circle"])
    except getopt.GetoptError, err:
        usage()
        sys.exit(0)

    output = ""
    input = ""
    number = 1
    radius = 1.0
    circle = False
    for o, a in opts:
        if o in ("-o", "--output"):
            output = str(a)
        if o in ("-i", "--input"):
            input = str(a)
        if o in ("-n", "--number"):
            number = int(a)
            if number < 0: number = 1
        if o in ("-r", "--radius"):
            radius = float(a)
            if radius < 0.0: radius = 1.0
        if o in ("-c", "--circle"):
            circle = True

    if output == "":
        usage()
        print ("No output file given. Abort!")
        sys.exit(0)

    print ("Using params output='%s', input='%s', number=%d, radius=%f, circle=%s" % (output, input, number, radius, circle))
    generateGroup(input, output, number, radius, circle)
