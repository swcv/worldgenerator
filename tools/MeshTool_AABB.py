#!/usr/bin/python
#
# Author: Jarkko Vatjus-Anttila <jvatjusanttila@gmail.com>
#
# For conditions of distribution and use, see copyright notice in license.txt
#

import sys, os
import MeshContainer
import MeshIO
import MeshGenerator

###############################################################################
#

if __name__ == "__main__":
    import getopt

    def usage():
        print ("MeshTool_Optimize -i <input> -o <output> -p <0-100> ")

    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:p:o:",
            ["input=", "percentage=", "output="])
    except getopt.GetoptError as err:
        usage()
        sys.exit(0)

    p = 100.0
    input = None
    output = None
    for o, a in opts:
        if o in ("-i", "--input"):
            input = str(a)
        if o in ("-p", "--percentage"):
            p = float(a)
            if p < 0.0: p=0.0
            if p > 100.0: p=100.0
        if o in ("-o", "--output"):
            output = str(a)

    if input == None:
        print ("No input file defined")
        usage()
        sys.exit(0)

    p = 1 - p/100.0

    mesh = MeshContainer.MeshContainer()
    meshio = MeshIO.MeshIO(mesh)
    meshio.fromFile(input, None)
    mesh.printStatistics()

    mesh.buildAABBMesh()
    mesh.printStatistics()

    if output != None:
        meshio = MeshIO.MeshIO(mesh)
        print ("Trying output write to %s" % output)
        meshio.toFile(output, overwrite=True)

