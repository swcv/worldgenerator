#!/usr/bin/env python3
#
# Author: Jarkko Vatjus-Anttila <jvatjusanttila@gmail.com>
#
# For conditions of distribution and use, see copyright notice in license.txt
#

import sys, os
import MeshContainer
import MeshIO
import MeshGenerator
import TerrainGenerator
import TextureGenerator

###############################################################################
#

if __name__ == "__main__":
    import getopt

    def usage():
        print ("MeshTool_TerrainTriangulation -i <input> -o <output> -p <0-100> -v")

    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:p:o:v",
            ["input=", "percentage=", "output=", "verbose"])
    except getopt.GetoptError as err:
        usage()
        sys.exit(0)

    p = 100.0
    input = ""
    output = None
    verbose = False
    for o, a in opts:
        if o in ("-i", "--input"):
            input = str(a)
        if o in ("-p", "--percentage"):
            p = float(a)
            if p < 0.0: p=0.0
            if p > 100.0: p=100.0
        if o in ("-o", "--output"):
            output = str(a)
        if o in ("-v", "--verbose"):
            verbose = True

    p = 1 - p/100.0

    mesh = MeshContainer.MeshContainer()
    meshio = MeshIO.MeshIO(mesh)
    if verbose == True: mesh.enableVerbose()

    print ("Loading input file '%s'" % input)
    t = TerrainGenerator.TerrainGenerator()
    try: t.fromFile(input)
    except NameError:
        print ("Unable to open input '%s'" % input)
        usage()
        sys.exit(0)
    t.rescale(-5, 35)
    t.triangulate(mesh, gridSize=5.0)
    if p != 0.0:
        print ("Collapsing p=%f edges" % p)
        mesh.edgeCollapse(percentage=p)

    if output != None:
        meshio = MeshIO.MeshIO(mesh)
        print ("Trying output write to '%s'" % output)
        meshio.toFile(output, overwrite=True)

