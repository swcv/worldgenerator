#!/usr/bin/env python3
#
# Author: Jarkko Vatjus-Anttila <jvatjusanttila@gmail.com>
#
# For conditions of distribution and use, see copyright notice in license.txt
#

import sys, os
import MeshContainer
import MeshIO
import MeshGenerator

###############################################################################
#

if __name__ == "__main__":
    import getopt

    def usage():
        print ("MeshMergeTool -a <input1> -b <input2> [-o <outputfile>]")

    try:
        opts, args = getopt.getopt(sys.argv[1:], "a:b:O:o:",
            ["filea=", "fileb=", "output="])
    except getopt.GetoptError as err:
        usage()
        sys.exit(0)

    i1 = None
    i2 = None
    operation = "merge"
    output = None
    for o, a in opts:
        if o in ("-a", "--filea"):
            i1 = str(a)
        if o in ("-b", "--fileb"):
            i2 = str(a)
        if o in ("-o", "--output"):
            output = str(a)
        if o in ("-O", "--operation"):
            operation = str(a)

    if operation not in [ "merge", "append"]:
        print ("Invalid operationtype '%s'. Abort!" % operation)
        sys.exit(0)

    if i1 == None or i2 == None:
        print ("No input given. Abort!")
        usage()
        sys.exit(0)

    try:
        mesh = MeshContainer.MeshContainer()
        meshio = MeshIO.MeshIO(mesh)
        meshio.fromFile(i1, None)
        mesh.printStatistics()

        mesh2 = MeshContainer.MeshContainer()
        meshio2 = MeshIO.MeshIO(mesh2)
        meshio2.fromFile(i2, None)
        mesh.printStatistics()

        if operation == "merge":
            rc = mesh.merge(mesh2)
        elif operation == "append":
            rc = mesh.append(mesh2)

        if rc == -1:
            print ("%s operation failed. Abort!" % operation)
            sys.exit(0)
        mesh.printStatistics()
        if output != None:
            print ("Trying output write to %s" % output)
            meshio.toFile(output, overwrite=True)

    except IOError:
        print ("Error parsing the input file!")
    except OSError:
        print ("OSError, check OgreXMLConverter installation?")
