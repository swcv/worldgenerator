#!/usr/bin/python
#
# Author: Jarkko Vatjus-Anttila <jvatjusanttila@gmail.com>
#
# For conditions of distribution and use, see copyright notice in license.txt
#

import sys, os
import MeshIO
import MeshContainer
import WorldGenerator

###############################################################################
#

if __name__ == "__main__":
    import getopt

    def usage():
        print ("MeshTool_MeshToTXML -i <inputMESH> -o <outputTXML> -O|--overwrite")

    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:o:mO",
            ["input=", "output=", "overwrite"])
    except getopt.GetoptError as err:
        usage()
        sys.exit(0)

    input = ""
    output = None
    overwrite = False
    for o, a in opts:
        if o in ("-i", "--input"):
            input = str(a)
        if o in ("-o", "--output"):
            output = str(a)
        if o in ("-O", "--overwrite"):
            overwrite = True

    if input == "":
        usage()
        print ("No input given!")
        sys.exit(0)

    mesh = MeshContainer.MeshContainer()
    meshio = MeshIO.MeshIO(mesh)
    print ("Loading '%s'" % input)
    meshio.fromFile(input, "model/x-ogremesh")

    materialnames = mesh.getMaterialNames()
    TXMLmaterials = ""
    for m in materialnames:
        TXMLmaterials += m+".material;"
    TXMLmaterials = TXMLmaterials[:-1]

    if output != None:
        print ("Building the TXML scene")
        world = WorldGenerator.WorldGenerator()
        world.TXML.startScene()
        world.createEntity_Staticmesh(1, "Object"+str(world.TXML.getCurrentEntityID()),
                                      mesh="%s"%input[:-4],
                                      material=TXMLmaterials,
                                      transform="0,0,0,0,0,0,1,1,1")
        world.TXML.startEntity()
        world.createComponent_Name(1, {"name":"Defaultlight"})
        world.createComponent_Placeable(1, {})
        world.createComponent_Light(1, {})
        world.TXML.endEntity()
        world.TXML.endScene()
        world.toFile(output, overwrite=overwrite)

print ("Done!")


