#!/usr/bin/env python3
#
# Author: Jarkko Vatjus-Anttila <jvatjusanttila@gmail.com>
#
# For conditions of distribution and use, see copyright notice in license.txt
#

import sys, os
import MeshContainer
import MeshIO
import MeshGenerator

###############################################################################
#

if __name__ == "__main__":
    import getopt

    def usage():
        print ("MeshTool_Info -i|--input <input> -o|--output <output> -n|--normals -r|--rotate <rotation> -s|--uscale <scale> -S|--scale <scale> -c|--collapse -d|--deadfaces -v|--verbose -D|--duplicatevertices")
        print (" -i input file")
        print (" -o output file")
        print (" -v verbose output")
        print (" -n recalculate mesh normals")
        print (" -s perform uniform scaling in object space coordinates")
        print (" -S perform axle -wise scaling <x, y, z>")
        print (" -r apply rotation axle -wize <x, y, z>")
        print (" -d remove dead and empty faces")
        print (" -D remove duplicate vertices (default radius=0.0)")
        print (" -R custom radius for removal operations")
        print (" -c collapse similar materials into single submesh")

    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:o:nr:s:S:cdvDR:",
            ["input=", "output=", "normals", "rotation=", "uscale=", "scale=", "collapse=", "collapse", "deadfaces", "verbose", "duplicatevertices", "radius="])
    except getopt.GetoptError as err:
        usage()
        sys.exit(0)

    input               = ""
    output              = None
    normals             = False
    scaleF              = None
    rotateF             = None
    collapse            = False
    deadfaces           = False
    duplicatevertices   = False
    verbose = 0
    radius = 0.0
    for o, a in opts:
        if o in ("-i", "--input"):
            input = str(a)
        if o in ("-o", "--output"):
            output = str(a)
        if o in ("-n", "--normals"):
            normals = True
        if o in ("-s", "--uscale"):
            scale = float(a)
            if scale < 0.0: scale=0.01
            scaleF = [ scale for a in range(3) ]
        if o in ("-S", "--scale"):
            scaleF = [ float(x) for x in a.split(',') ]
        if o in ("-r", "--rotate"):
            rotateF = [ float(x) for x in a.split(',') ]
        if o in ("-c", "--collapse"):
            collapse = True
        if o in ("-d", "--deadfaces"):
            deadfaces = True
        if o in ("-D", "--duplicatevertices"):
            duplicatevertices = True
        if o in ("-R", "--radius"):
            radius = float(a)
            if radius < 0.0: radius=0.0
        if o in ("-v", "--verbose"):
            verbose = 1

    mesh = MeshContainer.MeshContainer()
    if verbose == 1: mesh.enableVerbose()
    meshio = MeshIO.MeshIO(mesh)

    print ("Loading mesh '%s'" % input)
    try: meshio.fromFile(input, None)
    except NameError:
        print ("Error!")
        usage()
        sys.exit(0)
    mesh.printStatistics()

    if output != None:
        meshio = MeshIO.MeshIO(mesh)
        if collapse == True:
            print ("Collapsing identical materials")
            mesh.collapseSimilars()
        if duplicatevertices == True:
            print ("Removing duplicate vertices")
            mesh.removeDuplicateVertices(radius=radius)
        if deadfaces == True:
            print ("Removing dead faces")
            mesh.removeDeadFaces()
        if scaleF != None:
            print ("Applying scale factor of %s" % scaleF)
            mesh.scale(scaleF[0], scaleF[1], scaleF[2])
        if rotateF != None:
            print ("Applying rotation %s" % rotateF)
            mesh.rotate(rotateF[0], 1, 0, 0)
            mesh.rotate(rotateF[1], 0, 1, 0)
            mesh.rotate(rotateF[2], 0, 0, 1)
        if normals == True:
            print ("Recalculating normal vectors")
            mesh.recalculateNormals()
        print ("Trying output write to %s" % output)
        meshio.toFile(output, overwrite=True)

