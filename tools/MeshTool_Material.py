#!/usr/bin/python
#
# Author: Jarkko Vatjus-Anttila <jvatjusanttila@gmail.com>
#
# For conditions of distribution and use, see copyright notice in license.txt
#

import sys, os
import MaterialGenerator

###############################################################################
#

if __name__ == "__main__":
    import getopt

    def usage():
        print ("MeshTool_Material -i <input> -o <output> -m|--merge -O|--overwrite -c|--countduplicates")

    try:
        opts, args = getopt.getopt(sys.argv[1:], "i:o:mOc",
            ["input=", "output=", "merge", "overwrite", "countduplicates" ])
    except getopt.GetoptError as err:
        usage()
        sys.exit(0)

    input = ""
    output = None
    merge = False
    overwrite = False
    countduplicates = False
    for o, a in opts:
        if o in ("-i", "--input"):
            input = str(a)
        if o in ("-o", "--output"):
            output = str(a)
        if o in ("-m", "--merge"):
            merge = True
        if o in ("-O", "--overwrite"):
            overwrite = True
        if o in ("-c", "--countduplicates"):
            countduplicates = True

    mc = MaterialGenerator.MaterialContainer()
    try:
        mc.fromFile(input)
    except IOError:
        print ("IOError when opening '%s'" % input)
        usage()
        sys.exit(0)

    if countduplicates == True:
        mc.countDuplicates()

    if output != None:
        if merge == True: print ("Writing output to '%s'" % output)
        mc.toFile(output, overwrite=overwrite, append=merge)

    print ("Done!")


