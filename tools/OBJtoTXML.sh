#!/bin/bash

echo "Usage: $0 <OBJFILE> <MTLFILE>"
MeshTool_Info.py -i $1 -o $1.mesh.xml -c -n -v -d
OgreXMLConverter $1.mesh.xml
MeshTool_Material.py -i $2 -o default.material -O
MeshTool_MeshToTXML.py -i $1.mesh.xml -o test.txml -O

