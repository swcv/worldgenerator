#!/usr/bin/env python3
#
# Author: Jarkko Vatjus-Anttila <jvatjusanttila@gmail.com>
#
# For conditions of distribution and use, see copyright notice in license.txt
#

import sys, os
import MeshContainer
import MeshIO
import MeshGenerator

###############################################################################
#

if __name__ == "__main__":
    import getopt

    def usage():
        print ("MeshGeneratorTool [options]")
        print (" -t|--type       Target mesh type. One of: cube, sphere, cylinder, plane")
        print (" -l|--lod        Target LOD level [1-N]")
        print (" -s|--shared     Generate shared geometry [true/false]")
        print (" -o|--output     Target filename")
        print (" -n|--normals    Recalculate normals for the generated mesh")
        print (" -d|--deadfaces  Remove dead faces from the geometry")

    try:
        opts, args = getopt.getopt(sys.argv[1:], "t:l:so:nd",
            ["type=", "lod=", "shared", "output=", "normals", "deadfaces"])
    except getopt.GetoptError as err:
        usage()
        sys.exit(0)

    t = ""
    l = 1
    output = None
    shared = False
    normals = False
    deadfaces = False
    for o, a in opts:
        if o in ("-t", "--type"):
            t = str(a)
        if o in ("-l", "--lod"):
            l = int(a)
            if l < 1: l=1
        if o in ("-s", "shared"):
            shared = True
        if o in ("-o", "--output"):
            output = str(a)
        if o in ("-n", "--normals"):
            normals = True
        if o in ("-d", "--deadfaces"):
            deadfaces = True

    if t == "":
        usage()
        print ("No type parameter given. Abort!")
        sys.exit(0)

    mesh = MeshContainer.MeshContainer()
    meshgen = MeshGenerator.MeshGenerator(mesh, sharedgeometry=shared)
    if t == "cube":
        meshgen.createCube(LOD=l)
    elif t == "plane":
        meshgen.createPlane(LOD=l)
    elif t == "sphere":
        meshgen.createSphere(LOD=l)
    elif t == "cylinder":
        meshgen.createCylinder(LOD=l)
    else:
        print ("Invalid type parameter '%s'. Abort! (supported 'cube', 'plane', 'sphere', 'cylinder'" % t)
        sys.exit(0)

    if output != None:
        meshio = MeshIO.MeshIO(mesh)
        if deadfaces == True:
            mesh.removeDeadFaces()
        if normals == True:
            mesh.recalculateNormals()
        print ("Trying output write to %s" % output)
        meshio.toFile(output, overwrite=True)
